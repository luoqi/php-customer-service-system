客服管理系统
===============

> 运行环境推荐PHP8.0+，基于thinkphp8.x前后端分离架构，开箱即用

## 简介
* 前端采用 uniapp+全新UI
* 后端采用 thinkphp8 + mysql8 + vue 技术
* 完善的权限认证：菜单、按钮、api、登录授权
* 日志：登录日志、操作日志、系统日志
* thinkphp8.x 使用手册（https://doc.thinkphp.cn/v8_0/preface.html）



## 运行项目
``` sh
# 克隆项目
git clone https://gitee.com/source-code-home/php-customer-service-system.git

# 创建数据库并把service.sql导入数据库中

# 进入项目目录
cd qihe-admin

# 启动项目(开发模式)
php think worker:gateway -d



```
启动完成后浏览器访问 http://localhost:8000/admin


## 效果展示
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20240622162800.png)
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20240622153658.png)
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20240622153634.png)
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20240622150136.png)
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20240622153739(1)(1).png)




## 提交反馈

* QQ交流群：621151094
* 技术交流，获得更多开源资料，请联系

![输入图片说明](700cad.png)




## 特别鸣谢

* 感谢 [thinkphp] (http://thinkphp.cn)


## 版权说明

* 代码可用于个人项目等接私活或企业项目, 免费开源
* 不允许二次开源收费

## 支持作者

如果对你有帮助，请点个Star，这将是对我最大的支持