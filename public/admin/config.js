
// 此文件非必要，在生产环境下此文件配置可覆盖运行配置，开发环境下不起效
// 详情见 src/config/index.js

const APP_CONFIG = {
	//标题
	APP_NAME: "客服",
	//接口地址，如遇跨域需使用nginx代理
	API_URL:"替换成你的域名",   //例如  "https://kf.shareadd.cn',
	WS_URL:"ws://127.0.0.1:2346" //将127.0.0.1  替换成你的域名
}
