-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2024-07-12 10:37:13
-- 服务器版本： 5.7.44-log
-- PHP 版本： 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `service2_shreadd`
--

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_apply`
--

CREATE TABLE `la_kefu_apply` (
  `id` int(11) NOT NULL COMMENT 'id',
  `user_id` varchar(100) DEFAULT NULL COMMENT '客户id',
  `name` varchar(50) NOT NULL COMMENT '联系人',
  `phone` varchar(11) NOT NULL COMMENT '电话',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类型',
  `content` text NOT NULL COMMENT '内容',
  `created_at` datetime NOT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `delete_at` datetime DEFAULT NULL COMMENT '删除时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='留言申请表';

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_banword`
--

CREATE TABLE `la_kefu_banword` (
  `id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1显示 0不显示'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_chats`
--

CREATE TABLE `la_kefu_chats` (
  `id` int(11) NOT NULL,
  `uid` varchar(200) CHARACTER SET utf8 NOT NULL COMMENT '访客id',
  `kefu_id` int(11) NOT NULL COMMENT '客服id',
  `fileType` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'text' COMMENT '消息类型。默认为text文本',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '内容',
  `state` int(1) DEFAULT '1' COMMENT '1是未读，2是已读',
  `direction` int(1) DEFAULT '1' COMMENT '1是发给客服，2是发给用户,3是转接客服',
  `switch_id` int(11) DEFAULT NULL COMMENT '转接客服ID',
  `unstr` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '前端唯一字符串用于撤销使用',
  `created_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='消息表' ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_comment`
--

CREATE TABLE `la_kefu_comment` (
  `id` int(11) NOT NULL,
  `kefu_id` int(11) DEFAULT NULL COMMENT '客服ID',
  `uid` varchar(100) DEFAULT NULL COMMENT '用户ID',
  `rate` int(1) DEFAULT NULL COMMENT '几颗星',
  `content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1显示 0不显示',
  `created_at` int(11) DEFAULT NULL COMMENT '评论时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_group`
--

CREATE TABLE `la_kefu_group` (
  `id` int(11) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1是客服，2是用户',
  `groupname` varchar(255) DEFAULT NULL,
  `sort` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` int(11) DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_product`
--

CREATE TABLE `la_kefu_product` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `product_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `created_at` int(11) DEFAULT NULL COMMENT '时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='常见问题表' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_robot`
--

CREATE TABLE `la_kefu_robot` (
  `id` int(11) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '默认为1,1是问题答案，2是快捷回复',
  `kefu_id` int(11) DEFAULT NULL COMMENT '客服ID',
  `qustion` varchar(100) DEFAULT NULL COMMENT '常见问题',
  `keyword` varchar(12) NOT NULL DEFAULT '' COMMENT '关键词',
  `reply` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='常见问题表' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_sentence`
--

CREATE TABLE `la_kefu_sentence` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL COMMENT '内容',
  `kefu_id` int(11) NOT NULL COMMENT '所属客服id',
  `state` enum('using','unuse') NOT NULL DEFAULT 'using' COMMENT 'unuse: 未使用 ，using：使用中'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `la_kefu_sentence`
--

INSERT INTO `la_kefu_sentence` (`id`, `content`, `kefu_id`, `state`) VALUES
(3, '您好，很高兴为您服务，您有什么需要咨询的吗3？', 2, 'using');

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_setting`
--

CREATE TABLE `la_kefu_setting` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '网站名称',
  `logo` varchar(100) DEFAULT NULL COMMENT '网站logo',
  `domain` varchar(100) DEFAULT NULL,
  `ws_url` varchar(100) DEFAULT NULL,
  `file_path` varchar(150) DEFAULT NULL COMMENT '文件路径',
  `is_off` int(1) DEFAULT '1' COMMENT '1是开启，2是关闭',
  `worktime` varchar(100) DEFAULT NULL COMMENT '在线时间',
  `is_auto` int(11) NOT NULL DEFAULT '1' COMMENT '默认为1,1是智能分配，2是进入排队',
  `is_content` int(1) DEFAULT NULL COMMENT '1是开启，2是关闭',
  `is_window` int(1) NOT NULL DEFAULT '1' COMMENT '窗口设置，1是小窗口，2是大窗口',
  `is_float` int(1) NOT NULL DEFAULT '1' COMMENT '悬浮位置，1是底部悬浮，2是右侧悬浮	',
  `float_color` varchar(100) DEFAULT NULL COMMENT '悬浮条背景色',
  `home_color` varchar(100) DEFAULT NULL COMMENT '主题色',
  `sms_config` text COMMENT '短信配置',
  `sms_default` varchar(100) DEFAULT NULL COMMENT '默认阿里云短信',
  `email_config` text COMMENT '邮箱配置',
  `is_email` int(1) DEFAULT '1' COMMENT '默认为1,1是关闭，2是开启',
  `version` varchar(100) DEFAULT '1.0.0' COMMENT '版本号',
  `secret_key` varchar(100) DEFAULT NULL COMMENT '秘钥'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商家表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `la_kefu_setting`
--

INSERT INTO `la_kefu_setting` (`id`, `name`, `logo`, `domain`, `ws_url`, `file_path`, `is_off`, `worktime`, `is_auto`, `is_content`, `is_window`, `is_float`, `float_color`, `home_color`, `sms_config`, `sms_default`, `email_config`, `is_email`, `version`, `secret_key`) VALUES
(1, '客服系统', 'uploads/images/20240705/20240705182917cefaa8248.png', 'http://cservice.shareadd.cn', 'ws://cservice.shareadd.cn:2345', NULL, 1, '', 1, 1, 1, 1, NULL, NULL, NULL, 'smsbao', NULL, 2, '1.0.8', '');

-- --------------------------------------------------------

--
-- 表的结构 `la_kefu_user`
--

CREATE TABLE `la_kefu_user` (
  `id` int(11) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT '访客id',
  `kefu_id` int(11) DEFAULT NULL COMMENT '客服ID',
  `group_id` int(11) DEFAULT NULL COMMENT '分组ID',
  `avatar` varchar(1024) DEFAULT NULL COMMENT '头像',
  `name` varchar(255) DEFAULT '' COMMENT '用户自己填写的姓名',
  `tel` varchar(32) DEFAULT '' COMMENT '用户自己填写的电话',
  `login_num` int(11) DEFAULT '1' COMMENT '登录次数',
  `connect` text COMMENT '联系方式',
  `comment` text COMMENT '备注',
  `extends` text COMMENT '浏览器扩展',
  `ip` varchar(255) DEFAULT NULL COMMENT '访客ip',
  `from_url` varchar(255) DEFAULT NULL COMMENT '访客浏览地址',
  `msg_time` timestamp NULL DEFAULT NULL,
  `created_at` varchar(100) DEFAULT NULL COMMENT '访问时间',
  `state` int(1) DEFAULT '1' COMMENT '1：正常接入,2:已经解决，3:黑名单',
  `is_top` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '1置顶展示0未置顶',
  `is_online` int(1) DEFAULT '1' COMMENT '1是在线，0是离线',
  `product` text COMMENT '商品信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `la_system_dept`
--

CREATE TABLE `la_system_dept` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `parent_id` int(11) DEFAULT '0' COMMENT '上级ID',
  `label` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `name` varchar(30) NOT NULL COMMENT '名称',
  `intro` varchar(255) DEFAULT NULL COMMENT '简介',
  `leader` varchar(50) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 (0正常 1停用)',
  `sort` tinyint(3) UNSIGNED DEFAULT '1' COMMENT '排序',
  `created_by` varchar(30) DEFAULT NULL COMMENT '创建者',
  `updated_by` varchar(30) DEFAULT NULL COMMENT '更新者',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员分组';

-- --------------------------------------------------------

--
-- 表的结构 `la_system_dic_data`
--

CREATE TABLE `la_system_dic_data` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `type_id` bigint(20) UNSIGNED NOT NULL COMMENT '字典类型ID',
  `data_type` varchar(10) DEFAULT 'text' COMMENT '数据类型：text文本、image图片',
  `label` varchar(50) DEFAULT NULL COMMENT '字典标签',
  `value` varchar(100) DEFAULT NULL COMMENT '字典值',
  `code` varchar(100) DEFAULT NULL COMMENT '字典标示',
  `sort` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '排序',
  `created_by` varchar(25) DEFAULT NULL COMMENT '创建者',
  `updated_by` varchar(25) DEFAULT NULL COMMENT '更新者',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典数据表';

--
-- 转存表中的数据 `la_system_dic_data`
--

INSERT INTO `la_system_dic_data` (`id`, `type_id`, `data_type`, `label`, `value`, `code`, `sort`, `created_by`, `updated_by`, `remark`, `created_at`, `updated_at`) VALUES
(1, 2, 'text', '1', '2', 'system_items', 1, 'superAdmin', 'superAdmin', '1', '2024-01-30 17:50:10', '2024-02-22 17:02:50');

-- --------------------------------------------------------

--
-- 表的结构 `la_system_dic_type`
--

CREATE TABLE `la_system_dic_type` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `parent_id` bigint(20) UNSIGNED DEFAULT '0' COMMENT '上级ID',
  `name` varchar(50) DEFAULT NULL COMMENT '字典名称',
  `code` varchar(100) DEFAULT NULL COMMENT '字典编码',
  `sort` tinyint(3) UNSIGNED DEFAULT '0' COMMENT '排序',
  `created_by` varchar(25) DEFAULT NULL COMMENT '创建者',
  `updated_by` varchar(25) DEFAULT NULL COMMENT '更新者',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典类型表';

--
-- 转存表中的数据 `la_system_dic_type`
--

INSERT INTO `la_system_dic_type` (`id`, `parent_id`, `name`, `code`, `sort`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 0, '文件存储', 'files_storage', 1, 'superAdmin', NULL, '2023-07-17 20:14:06', '2023-07-17 20:14:06'),
(2, 0, '系统配置', 'system_items', 1, 'superAdmin', NULL, '2023-07-17 20:18:32', '2023-07-17 20:18:32'),
(3, 0, '测试后', 'test', 1, 'superAdmin', NULL, '2024-01-30 17:51:55', '2024-01-30 17:51:55');

-- --------------------------------------------------------

--
-- 表的结构 `la_system_group`
--

CREATE TABLE `la_system_group` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `parent_id` int(11) DEFAULT '0' COMMENT '上级ID',
  `label` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `name` varchar(30) DEFAULT NULL COMMENT '名称',
  `leader` varchar(50) DEFAULT NULL COMMENT '负责人',
  `remark` text COMMENT '备注',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 (0正常 1停用)',
  `sort` tinyint(3) UNSIGNED DEFAULT '1' COMMENT '排序',
  `created_by` varchar(30) DEFAULT NULL COMMENT '创建者',
  `updated_by` varchar(30) DEFAULT NULL COMMENT '更新者',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员分组';

--
-- 转存表中的数据 `la_system_group`
--

INSERT INTO `la_system_group` (`id`, `parent_id`, `label`, `name`, `leader`, `remark`, `phone`, `status`, `sort`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(19, 0, '客服部门', NULL, 'test', NULL, '13988888888', 1, 100, NULL, NULL, '2024-07-05 18:47:42', '2024-07-05 18:47:42');

-- --------------------------------------------------------

--
-- 表的结构 `la_system_logger`
--

CREATE TABLE `la_system_logger` (
  `id` bigint(20) NOT NULL,
  `module` varchar(20) NOT NULL DEFAULT 'default' COMMENT '日志模块：1默认',
  `level` enum('notice','error','warning') NOT NULL DEFAULT 'notice' COMMENT '日志等级： notice、warning、error',
  `param` json DEFAULT NULL COMMENT '操作参数',
  `message` text COMMENT '日志信息',
  `tag` varchar(64) DEFAULT '' COMMENT '日志标签',
  `router` varchar(50) NOT NULL DEFAULT '' COMMENT '请求路由',
  `method` varchar(20) NOT NULL DEFAULT '' COMMENT '请求方式',
  `uid` bigint(20) UNSIGNED DEFAULT '0' COMMENT '操作用户',
  `port` enum('u','m') DEFAULT 'u' COMMENT '端口：u=服务端 m=客户端',
  `created_at` datetime DEFAULT NULL COMMENT '生成时间',
  `time_stamp` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统日志';

-- --------------------------------------------------------

--
-- 表的结构 `la_system_logger_login`
--

CREATE TABLE `la_system_logger_login` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `username` varchar(30) NOT NULL COMMENT '账户',
  `ip` varchar(30) DEFAULT NULL COMMENT '登录IP地址',
  `ip_location` varchar(180) DEFAULT NULL COMMENT 'IP所属地',
  `os` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `browser` varchar(50) DEFAULT NULL COMMENT '浏览器',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '登录状态 (0成功 1失败)',
  `message` varchar(50) DEFAULT NULL COMMENT '提示消息',
  `login_time` timestamp NOT NULL COMMENT '登录时间',
  `remark` varchar(160) DEFAULT NULL COMMENT '备注',
  `time_stamp` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='登录日志表';

--
-- 转存表中的数据 `la_system_logger_login`
--

INSERT INTO `la_system_logger_login` (`id`, `username`, `ip`, `ip_location`, `os`, `browser`, `status`, `message`, `login_time`, `remark`, `time_stamp`) VALUES
(25, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 07:12:57', '', 1720681977),
(26, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 07:32:37', '', 1720683157),
(27, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 08:12:48', '', 1720685568),
(28, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 08:13:56', '', 1720685636),
(29, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 08:14:31', '', 1720685671),
(30, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 08:16:00', '', 1720685760),
(31, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 10:11:24', '', 1720692684),
(32, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 10:18:06', '', 1720693086),
(33, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 10:20:47', '', 1720693247),
(34, 'admin', '111.175.58.60', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-11 10:45:32', '', 1720694732),
(35, 'admin', '219.140.116.96', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-12 01:33:30', '', 1720748010),
(36, 'admin', '219.140.116.96', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-12 02:22:30', '', 1720750950),
(37, 'admin', '219.140.116.96', NULL, 'Windows 10', '谷歌', 1, '登录成功', '2024-07-12 02:29:39', '', 1720751379);

-- --------------------------------------------------------

--
-- 表的结构 `la_system_logger_oper`
--

CREATE TABLE `la_system_logger_oper` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `method` varchar(20) NOT NULL COMMENT '请求方式',
  `router` varchar(255) NOT NULL COMMENT '请求路由',
  `service_name` varchar(30) NOT NULL COMMENT '业务名称',
  `ip` varchar(30) DEFAULT NULL COMMENT '请求IP地址',
  `ip_location` varchar(180) DEFAULT NULL COMMENT 'IP所属地',
  `request_data` json DEFAULT NULL COMMENT '请求数据',
  `response_code` varchar(5) DEFAULT NULL COMMENT '响应状态码',
  `response_data` json DEFAULT NULL COMMENT '响应数据',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `time_stamp` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='操作日志表';

-- --------------------------------------------------------

--
-- 表的结构 `la_system_menu`
--

CREATE TABLE `la_system_menu` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `parent_id` bigint(20) UNSIGNED DEFAULT '0' COMMENT '父ID',
  `title` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单别名',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `component` varchar(200) DEFAULT NULL COMMENT '组件路径',
  `path` varchar(255) DEFAULT NULL COMMENT '路由地址',
  `redirect` varchar(255) DEFAULT NULL COMMENT '跳转地址',
  `hidden` tinyint(4) NOT NULL DEFAULT '0' COMMENT '隐藏菜单 (1是 0否)',
  `type` char(10) NOT NULL DEFAULT '' COMMENT '菜单类型, (menu菜单 button按钮 link链接 iframe iframe)',
  `color` varchar(100) DEFAULT NULL COMMENT '颜色',
  `active` varchar(100) DEFAULT NULL COMMENT '菜单高亮',
  `hidden_breadcrumb` varchar(100) DEFAULT NULL COMMENT '隐藏面包屑 (1是 0否)',
  `affix` tinyint(4) DEFAULT '0' COMMENT '是否固定 (1是 0否)',
  `fullpage` tinyint(4) DEFAULT '0' COMMENT '是否整页打开路由(1是 0否)',
  `sort` tinyint(3) UNSIGNED DEFAULT '1' COMMENT '排序',
  `updated_by` varchar(25) DEFAULT NULL COMMENT '更新者',
  `created_by` varchar(25) DEFAULT NULL COMMENT '创建者',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单';

--
-- 转存表中的数据 `la_system_menu`
--

INSERT INTO `la_system_menu` (`id`, `parent_id`, `title`, `name`, `icon`, `component`, `path`, `redirect`, `hidden`, `type`, `color`, `active`, `hidden_breadcrumb`, `affix`, `fullpage`, `sort`, `updated_by`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 0, '首页', 'home', 'el-icon-ticket', 'home', '/dashboard', '', 0, 'menu', '', '', '0', 1, 0, 1, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-04-10 18:42:54'),
(2, 1, '统计分析', 'dashboard', 'el-icon-menu', 'home', '/dashboard', '', 0, 'menu', '', '', '0', 1, 0, 1, 'superAdmin', NULL, '2022-08-02 14:03:40', '2024-05-30 15:48:42'),
(4, 0, '系统', 'setting', 'el-icon-setting', 'setting', '/setting', '', 0, 'menu', '', '', '0', 0, 0, 100, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-04-10 18:37:58'),
(5, 4, '短信配置', 'system', 'el-icon-message', 'setting/system', '/setting/system', '', 0, 'menu', '', '', '0', 0, 0, 6, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-05-21 15:42:22'),
(6, 4, '客服管理', 'user', 'el-icon-user-filled', 'setting/user', '/setting/user', '', 0, 'menu', '', '', '0', 0, 0, 2, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-04-18 18:08:50'),
(7, 4, '角色管理', 'role', 'el-icon-notebook', 'setting/role', '/setting/role', '', 0, 'menu', '', '', '0', 0, 0, 5, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-04-10 10:06:03'),
(8, 4, '部门管理', 'usergroup', 'sc-icon-organization', 'setting/usergroup', '/setting/usergroup', '', 0, 'menu', '', '', '0', 0, 0, 3, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-04-10 10:06:06'),
(9, 4, '菜单管理', 'settingMenu', 'el-icon-fold', 'setting/menu', '/setting/menu', '', 0, 'menu', '', '', '0', 0, 0, 7, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-04-10 10:06:08'),
(10, 22, '系统日志', 'systemlog', 'el-icon-warning', 'setting/systemlog', '/setting/systemlog', '', 0, 'menu', '', '', '0', 0, 0, 12, 'systemAdmin', NULL, '2022-08-02 14:03:40', '2024-04-10 10:05:56'),
(13, 22, '登录日志', 'loginlog', 'el-icon-ice-cream', 'setting/loginlog', '/setting/loginlog', '', 0, 'menu', '', '', '0', 0, 0, 10, 'systemAdmin', 'superAdmin', '2022-08-03 17:26:16', '2024-04-10 10:05:51'),
(14, 22, '操作日志', 'operlog', 'el-icon-edit-pen', 'setting/operlog', '/setting/operlog', '', 0, 'menu', '', '', '0', 0, 0, 11, 'systemAdmin', 'superAdmin', '2022-08-03 17:26:36', '2024-04-10 10:05:54'),
(20, 4, '文件管理', 'resource', 'el-icon-document', 'setting/resource', '/setting/resource', '', 0, 'menu', '', '', '0', 0, 0, 5, 'systemAdmin', 'superAdmin', '2022-08-22 22:24:51', '2024-04-10 10:06:11'),
(22, 0, '日志', 'develop', 'el-icon-home-filled', 'develop', '/develop', '', 0, 'menu', '', '', '0', 0, 0, 99, 'systemAdmin', '', '2022-08-28 15:45:02', '2024-04-10 18:37:47'),
(38, 6, '新增/修改', 'useredit', '', '', '', '', 0, 'button', '', '', '0', 0, 0, 1, NULL, 'superAdmin', '2022-10-10 21:30:24', '2022-10-10 21:30:24'),
(39, 4, '存储管理', 'storage', 'el-icon-connection', 'setting/storage', '/setting/storage', '', 0, 'menu', '', '', '0', 0, 0, 1, NULL, 'systemAdmin', '2024-04-10 10:10:28', '2024-04-10 10:10:28'),
(47, 0, '客服', 'server', 'el-icon-avatar', 'server', '/server', '', 0, 'menu', '', '', '0', 0, 0, 1, NULL, 'systemAdmin', '2024-04-16 10:53:26', '2024-04-16 10:53:26'),
(50, 47, '客户', 'consumer', 'el-icon-avatar', 'server/consumer', '/server/consumer', '', 0, 'menu', '', '', '0', 0, 0, 9, '开发者2344444', 'systemAdmin', '2024-04-16 14:10:15', '2024-06-14 11:30:45'),
(51, 47, '违禁词', 'banword', 'el-icon-delete', 'server/banword', '/server/banword', '', 0, 'menu', '', '', '0', 0, 0, 110, '开发者2344444', 'systemAdmin', '2024-04-16 16:54:28', '2024-06-14 11:33:06'),
(52, 47, '快捷回复', 'robot', 'el-icon-clock', 'server/robot', '/server/robot', '', 0, 'menu', '', '', '0', 0, 0, 80, '开发者2344444', 'systemAdmin', '2024-04-16 17:26:02', '2024-06-14 11:33:02'),
(53, 47, '问候语', 'sentence', 'el-icon-headset', 'server/sentence', '/server/sentence', '', 0, 'menu', '', '', '0', 0, 0, 12, '开发者2344444', 'systemAdmin', '2024-04-17 09:41:09', '2024-06-14 11:33:44'),
(56, 47, '聊天记录', 'chats', 'el-icon-chat-line-square', 'server/chats', '/server/chats', '', 0, 'menu', '', '', '0', 0, 0, 11, '开发者2344444', 'systemAdmin', '2024-04-18 11:22:11', '2024-06-14 11:33:41'),
(57, 47, '评价', 'comment', 'el-icon-chat-line-round', 'server/comment', '/server/comment', '', 0, 'menu', '', '', '0', 0, 0, 10, '开发者2344444', 'systemAdmin', '2024-04-18 17:03:14', '2024-06-14 11:30:32'),
(58, 4, '网站设置', 'setting', 'el-icon-setting', 'setting/setting', '/setting/setting', '', 0, 'menu', '', '', '0', 0, 0, 1, 'systemAdmin', 'systemAdmin', '2024-04-18 18:38:42', '2024-06-04 16:23:48'),
(59, 0, '聊天', 'chat', 'el-icon-user', 'chat', '/chat', '', 0, 'menu', '', '', '0', 0, 0, 1, NULL, 'systemAdmin', '2024-04-23 09:45:34', '2024-04-23 09:45:34'),
(60, 4, '邮箱配置', 'email', 'el-icon-promotion', 'setting/email', '/setting/email', '', 0, 'menu', '', '', '0', 0, 0, 1, NULL, 'systemAdmin', '2024-05-21 15:47:41', '2024-05-21 15:47:41'),
(66, 1, '帐号信息', 'userCenter', 'el-icon-user', 'userCenter', '/usercenter', '', 0, 'menu', '', '', '1', 0, 0, 1, 'systemAdmin', 'superAdmin', '2024-05-30 16:00:08', '2024-06-12 12:02:56'),
(67, 47, '常见问题', 'question', 'el-icon-bell', 'server/question', '/server/question', '', 0, 'menu', '', '', '0', 0, 0, 100, '开发者2344444', 'systemAdmin', '2024-06-11 11:38:45', '2024-06-14 11:30:15'),
(69, 4, '客服设置', 'service', 'el-icon-headset', 'setting/service', '/setting/service', '', 0, 'menu', '', '', '0', 0, 0, 1, NULL, '开发者2344444', '2024-06-13 17:41:18', '2024-06-13 17:41:18'),
(75, 47, '留言', 'serverApply', 'el-icon-chat-dot-square', 'server/apply', '/server/apply', '', 0, 'menu', '', '', '0', 0, 0, 1, NULL, 'systemAdmin', '2024-06-18 18:00:33', '2024-06-18 18:00:33');

-- --------------------------------------------------------

--
-- 表的结构 `la_system_menu_api`
--

CREATE TABLE `la_system_menu_api` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `menu_id` bigint(20) UNSIGNED NOT NULL COMMENT '菜单ID',
  `code` varchar(128) NOT NULL COMMENT '标识',
  `url` varchar(64) NOT NULL COMMENT '接口url',
  `api_method` varchar(8) NOT NULL DEFAULT 'ALL' COMMENT '请求方式 (ALL所有 POST GET PUT DELETE)',
  `is_record` tinyint(4) DEFAULT '0' COMMENT '是否记录: 0=否 1是'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='接口信息表';

--
-- 转存表中的数据 `la_system_menu_api`
--

INSERT INTO `la_system_menu_api` (`id`, `menu_id`, `code`, `url`, `api_method`, `is_record`) VALUES
(4, 38, ':system.user:edit', '/system.user/edit', 'POST', 1),
(7, 23, 'user:id', 'user/id', 'GET', 0);

-- --------------------------------------------------------

--
-- 表的结构 `la_system_notice`
--

CREATE TABLE `la_system_notice` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `type` tinyint(4) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `content` text COMMENT '公告内容',
  `read_num` int(11) DEFAULT '0' COMMENT '阅览次数',
  `created_by` varchar(25) DEFAULT NULL COMMENT '创建者',
  `updated_by` varchar(25) DEFAULT NULL COMMENT '更新者',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统公告表';

-- --------------------------------------------------------

--
-- 表的结构 `la_system_notice_read`
--

CREATE TABLE `la_system_notice_read` (
  `id` int(11) NOT NULL,
  `notice_id` int(11) NOT NULL COMMENT '系统公告id',
  `user_id` int(11) NOT NULL COMMENT '管理员id',
  `is_read` enum('y','n') DEFAULT 'n' COMMENT '查阅: y=已查阅  n=未查阅',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统公告查阅';

-- --------------------------------------------------------

--
-- 表的结构 `la_system_resource`
--

CREATE TABLE `la_system_resource` (
  `id` int(11) NOT NULL,
  `group` char(5) NOT NULL COMMENT '分组：A=服务端  B=客户端',
  `account_id` int(11) DEFAULT NULL COMMENT '账号id',
  `type` enum('image','video','audio','other') NOT NULL DEFAULT 'image' COMMENT '类型',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `size` varchar(20) DEFAULT NULL COMMENT '文件大小',
  `path` varchar(120) DEFAULT NULL COMMENT '保存路径',
  `ip` varchar(20) DEFAULT NULL COMMENT '上传ip地址',
  `notes` varchar(100) DEFAULT NULL COMMENT '备注',
  `deletable` tinyint(1) DEFAULT '0' COMMENT '可删除: 1=是，0=否',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文件资源';

--
-- 转存表中的数据 `la_system_resource`
--

INSERT INTO `la_system_resource` (`id`, `group`, `account_id`, `type`, `name`, `size`, `path`, `ip`, `notes`, `deletable`, `created_at`) VALUES
(352, 'A', 2, 'image', '66ddd99c75bc.png', '0.014MB', 'uploads/images/20240705/20240705182917cefaa8248.png', '111.175.59.123', NULL, 0, '2024-07-05 18:29:18');

-- --------------------------------------------------------

--
-- 表的结构 `la_system_role`
--

CREATE TABLE `la_system_role` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '主键，角色ID',
  `name` varchar(30) NOT NULL COMMENT '角色名称',
  `alias` varchar(100) NOT NULL COMMENT '角色别名',
  `data_scope` tinyint(4) DEFAULT '0' COMMENT '数据范围（0：全部数据权限 1：自定义数据权限 2：本部门数据权限 3：本部门及以下数据权限 4：本人数据权限）',
  `status` varchar(2) DEFAULT '0' COMMENT '状态 (0正常 1停用)',
  `sort` tinyint(3) UNSIGNED DEFAULT '1' COMMENT '排序',
  `remark` varchar(250) DEFAULT NULL COMMENT '备注',
  `created_by` varchar(30) DEFAULT NULL COMMENT '创建者',
  `updated_by` varchar(30) DEFAULT NULL COMMENT '更新者',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `delete_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';

--
-- 转存表中的数据 `la_system_role`
--

INSERT INTO `la_system_role` (`id`, `name`, `alias`, `data_scope`, `status`, `sort`, `remark`, `created_by`, `updated_by`, `created_at`, `updated_at`, `delete_at`) VALUES
(1, '超级管理员', 'superAdmin', 0, '1', 1, '系统内置角色，不可删除', '18397724129440', 'systemAdmin', '2022-07-05 21:48:05', '2024-05-28 16:00:48', NULL),
(2, '普通管理员', 'ordinaryAdmin', 2, '0', 1, '6', 'superAdmin', 'systemAdmin', '2022-09-26 17:33:51', '2024-02-23 14:55:22', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `la_system_role_dept`
--

CREATE TABLE `la_system_role_dept` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色主键',
  `dept_id` bigint(20) UNSIGNED NOT NULL COMMENT '部门主键'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色与部门关联表';

-- --------------------------------------------------------

--
-- 表的结构 `la_system_role_menu`
--

CREATE TABLE `la_system_role_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色主键',
  `menu_id` bigint(20) UNSIGNED NOT NULL COMMENT '菜单主键'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色与菜单关联表';

--
-- 转存表中的数据 `la_system_role_menu`
--

INSERT INTO `la_system_role_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 1, 21),
(22, 1, 22),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 38),
(33, 1, 39),
(34, 2, 1),
(35, 2, 2),
(36, 2, 3),
(41, 2, 20),
(43, 2, 22),
(44, 2, 4),
(45, 2, 15),
(46, 2, 6),
(47, 2, 38),
(48, 2, 39),
(49, 2, 8),
(50, 2, 7),
(51, 2, 5),
(52, 2, 9),
(53, 2, 13),
(54, 2, 14),
(55, 2, 10),
(56, 2, 11),
(97, 1, 44),
(98, 2, 44),
(99, 1, 42),
(100, 1, 41),
(103, 1, 46),
(104, 1, 47),
(105, 1, 48),
(106, 1, 49),
(107, 1, 50),
(108, 1, 51),
(109, 1, 52),
(110, 1, 53),
(111, 1, 54),
(112, 1, 55),
(113, 1, 56),
(114, 1, 57),
(115, 1, 58),
(116, 1, 59),
(138, 4, 41),
(139, 4, 58),
(140, 4, 48),
(141, 4, 47),
(142, 4, 46),
(143, 4, 42),
(144, 4, 57),
(145, 4, 52),
(146, 4, 59),
(147, 4, 56),
(148, 4, 55),
(149, 4, 54),
(150, 4, 53),
(151, 1, 64),
(152, 1, 65),
(153, 1, 66),
(154, 1, 67),
(155, 1, 68),
(156, 1, 69),
(157, 1, 70),
(158, 1, 40),
(159, 1, 43),
(160, 1, 45),
(161, 1, 60),
(162, 1, 61),
(163, 1, 62),
(164, 1, 63),
(165, 1, 71),
(167, 1, 74),
(168, 1, 75);

-- --------------------------------------------------------

--
-- 表的结构 `la_system_storage`
--

CREATE TABLE `la_system_storage` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT '名称',
  `tags` varchar(20) DEFAULT NULL COMMENT '标签',
  `bucket` varchar(100) DEFAULT NULL COMMENT '存储空间名',
  `access_key` varchar(100) NOT NULL COMMENT 'access_key',
  `secret_key` varchar(100) NOT NULL COMMENT 'secret_key',
  `domain` varchar(100) NOT NULL COMMENT '域名',
  `region` varchar(100) NOT NULL COMMENT 'region',
  `status` varchar(1) NOT NULL DEFAULT '0' COMMENT '1启用0关闭',
  `created_at` datetime NOT NULL COMMENT '创建时间',
  `updated_at` datetime NOT NULL COMMENT '更新时间',
  `delete_at` datetime NOT NULL COMMENT '删除时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='存储设置';

--
-- 转存表中的数据 `la_system_storage`
--

INSERT INTO `la_system_storage` (`id`, `name`, `tags`, `bucket`, `access_key`, `secret_key`, `domain`, `region`, `status`, `created_at`, `updated_at`, `delete_at`) VALUES
(1, '本地存储', 'local', NULL, '', '', '', '', '1', '2024-04-10 11:12:08', '2024-06-14 11:23:31', '2024-04-10 11:12:08'),
(2, 'qiniu', 'qiniuTags', 'qiniuBucket1', 'qiniuAccessKey1', 'qiniuSecretKey1', 'qiniuDomain1', '', '0', '2024-04-10 11:12:08', '2024-06-14 10:55:24', '2024-04-10 11:12:08'),
(3, '阿里云存储', 'aliyun', '31111', '41111', '51111', '61111', '', '0', '2024-04-10 11:12:08', '2024-06-14 10:55:37', '2024-04-10 11:12:08'),
(4, '腾讯云存储', 'qcloud', '5', '5', '5', '5', '5', '0', '2024-04-10 11:12:08', '2024-04-10 14:55:42', '2024-04-10 11:12:08');

-- --------------------------------------------------------

--
-- 表的结构 `la_system_user`
--

CREATE TABLE `la_system_user` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '用户ID',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `nickname` varchar(30) DEFAULT NULL COMMENT '用户昵称',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `avatar` varchar(255) DEFAULT 'storage/temp/202401/8158474eced3316d8074cbf68a8cb8a1.jpg' COMMENT '用户头像',
  `signed` varchar(255) DEFAULT NULL COMMENT '个人签名',
  `dashboard` varchar(100) DEFAULT NULL COMMENT '后台首页类型',
  `role_id` varchar(100) DEFAULT '0' COMMENT '角色ID',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门id',
  `group_id` varchar(100) DEFAULT NULL COMMENT '管理员分组id',
  `gid` varchar(100) DEFAULT NULL COMMENT '部门ID',
  `sex` tinyint(4) DEFAULT '0' COMMENT '性别：0保密 1男 2女',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '登录ip',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态 (0正常 1停用)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `created_by` varchar(25) DEFAULT NULL COMMENT '创建者',
  `updated_by` varchar(25) DEFAULT NULL COMMENT '更新者',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `delete_at` int(11) DEFAULT NULL COMMENT '软删除',
  `is_reply` int(1) DEFAULT '1' COMMENT '	1是第一条，2是随机，3是关闭',
  `state` int(1) DEFAULT '2' COMMENT '1是在线，2是离线'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员';

--
-- 转存表中的数据 `la_system_user`
--

INSERT INTO `la_system_user` (`id`, `username`, `nickname`, `password`, `phone`, `email`, `avatar`, `signed`, `dashboard`, `role_id`, `dept_id`, `group_id`, `gid`, `sex`, `login_ip`, `status`, `remark`, `created_by`, `updated_by`, `created_at`, `updated_at`, `delete_at`, `is_reply`, `state`) VALUES
(2, 'admin', '系统超管账号', 'e10adc3949ba59abbe56e057f20f883e', '13800000000', '', 'storage/temp/202401/8158474eced3316d8074cbf68a8cb8a1.jpg', '你好', '', '[1]', NULL, '1', '1', 2, '219.140.116.96', 1, '', 'superAdmin', 'superAdmin', '2022-10-09 20:19:40', '2024-07-12 10:29:39', NULL, 1, 2);

-- --------------------------------------------------------

--
-- 表的结构 `la_system_user_group`
--

CREATE TABLE `la_system_user_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户主键',
  `group_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色主键',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色与部门关联表';

--
-- 转存表中的数据 `la_system_user_group`
--

INSERT INTO `la_system_user_group` (`id`, `user_id`, `group_id`, `created_at`) VALUES
(22, 24, 8, '2024-04-29 16:10:26'),
(23, 24, 4, '2024-04-29 16:10:26'),
(24, 24, 9, '2024-04-29 16:10:26'),
(27, 25, 1, '2024-05-27 16:51:40'),
(29, 27, 8, '2024-05-27 16:58:03'),
(33, 26, 8, '2024-05-27 17:00:31'),
(34, 29, 1, '2024-05-28 15:38:50'),
(35, 29, 6, '2024-05-28 15:38:50'),
(36, 30, 1, '2024-05-28 15:43:09'),
(37, 30, 6, '2024-05-28 15:43:09'),
(41, 32, 8, '2024-05-30 15:43:56'),
(43, 33, 1, '2024-05-30 15:47:52'),
(44, 34, 1, '2024-05-30 16:37:20'),
(45, 34, 6, '2024-05-30 16:37:20'),
(48, 35, 8, '2024-05-30 17:42:05'),
(54, 22, 1, '2024-05-31 14:36:43'),
(58, 31, 1, '2024-05-31 15:20:48'),
(59, 36, 1, '2024-05-31 15:58:46'),
(61, 39, 1, '2024-06-07 09:47:57'),
(63, 28, 1, '2024-06-07 18:20:52');

-- --------------------------------------------------------

--
-- 表的结构 `la_system_user_role`
--

CREATE TABLE `la_system_user_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户主键',
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色主键',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色与部门关联表';

--
-- 转存表中的数据 `la_system_user_role`
--

INSERT INTO `la_system_user_role` (`id`, `user_id`, `role_id`, `created_at`) VALUES
(4, 1, 1, NULL),
(22, 2, 1, '2024-04-29 16:10:26'),
(23, 2, 2, '2024-04-29 16:10:26'),
(24, 2, 4, '2024-04-29 16:10:26'),
(27, 3, 1, '2024-05-15 16:30:03'),
(28, 25, 1, '2024-05-27 16:51:40'),
(30, 27, 4, '2024-05-27 16:58:03'),
(34, 26, 2, '2024-05-27 17:00:31'),
(35, 29, 1, '2024-05-28 15:38:50'),
(36, 29, 2, '2024-05-28 15:38:50'),
(37, 30, 1, '2024-05-28 15:43:09'),
(38, 30, 2, '2024-05-28 15:43:09'),
(42, 32, 2, '2024-05-30 15:43:56'),
(44, 33, 2, '2024-05-30 15:47:52'),
(45, 34, 1, '2024-05-30 16:37:20'),
(46, 34, 2, '2024-05-30 16:37:20'),
(49, 35, 2, '2024-05-30 17:42:05'),
(55, 23, 1, '2024-05-31 14:36:02'),
(56, 23, 2, '2024-05-31 14:36:02'),
(58, 22, 2, '2024-05-31 14:36:43'),
(62, 31, 1, '2024-05-31 15:20:48'),
(63, 36, 4, '2024-05-31 15:58:46'),
(65, 39, 2, '2024-06-07 09:47:57'),
(69, 11, 2, '2024-06-07 18:20:18'),
(70, 28, 7, '2024-06-07 18:20:52'),
(71, 12, 2, '2024-06-07 18:21:58'),
(72, 12, 4, '2024-06-07 18:21:58');

--
-- 转储表的索引
--

--
-- 表的索引 `la_kefu_apply`
--
ALTER TABLE `la_kefu_apply`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_kefu_banword`
--
ALTER TABLE `la_kefu_banword`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `la_kefu_chats`
--
ALTER TABLE `la_kefu_chats`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `visiter_id` (`uid`) USING BTREE,
  ADD KEY `service_id` (`kefu_id`) USING BTREE,
  ADD KEY `unstr` (`unstr`) USING BTREE;

--
-- 表的索引 `la_kefu_comment`
--
ALTER TABLE `la_kefu_comment`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `la_kefu_group`
--
ALTER TABLE `la_kefu_group`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `la_kefu_product`
--
ALTER TABLE `la_kefu_product`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `la_kefu_robot`
--
ALTER TABLE `la_kefu_robot`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `la_kefu_sentence`
--
ALTER TABLE `la_kefu_sentence`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `se` (`kefu_id`) USING BTREE;

--
-- 表的索引 `la_kefu_setting`
--
ALTER TABLE `la_kefu_setting`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `la_kefu_user`
--
ALTER TABLE `la_kefu_user`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `id` (`uid`) USING BTREE,
  ADD KEY `visiter` (`uid`) USING BTREE,
  ADD KEY `time` (`created_at`) USING BTREE;

--
-- 表的索引 `la_system_dept`
--
ALTER TABLE `la_system_dept`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_dic_data`
--
ALTER TABLE `la_system_dic_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_dict_data_type_id_index` (`type_id`);

--
-- 表的索引 `la_system_dic_type`
--
ALTER TABLE `la_system_dic_type`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_group`
--
ALTER TABLE `la_system_group`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_logger`
--
ALTER TABLE `la_system_logger`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_logger_login`
--
ALTER TABLE `la_system_logger_login`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_logger_oper`
--
ALTER TABLE `la_system_logger_oper`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_menu`
--
ALTER TABLE `la_system_menu`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_menu_api`
--
ALTER TABLE `la_system_menu_api`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_api_group_id_index` (`menu_id`),
  ADD KEY `o_system_menu_api_api_method_IDX` (`api_method`,`code`) USING BTREE;

--
-- 表的索引 `la_system_notice`
--
ALTER TABLE `la_system_notice`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_notice_read`
--
ALTER TABLE `la_system_notice_read`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_resource`
--
ALTER TABLE `la_system_resource`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_role`
--
ALTER TABLE `la_system_role`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_role_dept`
--
ALTER TABLE `la_system_role_dept`
  ADD PRIMARY KEY (`id`),
  ADD KEY `o_system_role_dept_role_id_IDX` (`role_id`,`dept_id`) USING BTREE;

--
-- 表的索引 `la_system_role_menu`
--
ALTER TABLE `la_system_role_menu`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_storage`
--
ALTER TABLE `la_system_storage`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `la_system_user`
--
ALTER TABLE `la_system_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `system_user_username_unique` (`username`);

--
-- 表的索引 `la_system_user_group`
--
ALTER TABLE `la_system_user_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `o_system_role_dept_role_id_IDX` (`user_id`,`group_id`) USING BTREE;

--
-- 表的索引 `la_system_user_role`
--
ALTER TABLE `la_system_user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `o_system_role_dept_role_id_IDX` (`user_id`,`role_id`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `la_kefu_apply`
--
ALTER TABLE `la_kefu_apply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `la_kefu_banword`
--
ALTER TABLE `la_kefu_banword`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- 使用表AUTO_INCREMENT `la_kefu_chats`
--
ALTER TABLE `la_kefu_chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- 使用表AUTO_INCREMENT `la_kefu_comment`
--
ALTER TABLE `la_kefu_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `la_kefu_group`
--
ALTER TABLE `la_kefu_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `la_kefu_product`
--
ALTER TABLE `la_kefu_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- 使用表AUTO_INCREMENT `la_kefu_robot`
--
ALTER TABLE `la_kefu_robot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- 使用表AUTO_INCREMENT `la_kefu_sentence`
--
ALTER TABLE `la_kefu_sentence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- 使用表AUTO_INCREMENT `la_kefu_setting`
--
ALTER TABLE `la_kefu_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `la_kefu_user`
--
ALTER TABLE `la_kefu_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `la_system_dept`
--
ALTER TABLE `la_system_dept`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=10;

--
-- 使用表AUTO_INCREMENT `la_system_dic_data`
--
ALTER TABLE `la_system_dic_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `la_system_dic_type`
--
ALTER TABLE `la_system_dic_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `la_system_group`
--
ALTER TABLE `la_system_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=20;

--
-- 使用表AUTO_INCREMENT `la_system_logger`
--
ALTER TABLE `la_system_logger`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `la_system_logger_login`
--
ALTER TABLE `la_system_logger_login`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=38;

--
-- 使用表AUTO_INCREMENT `la_system_logger_oper`
--
ALTER TABLE `la_system_logger_oper`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `la_system_menu`
--
ALTER TABLE `la_system_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=76;

--
-- 使用表AUTO_INCREMENT `la_system_menu_api`
--
ALTER TABLE `la_system_menu_api`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=8;

--
-- 使用表AUTO_INCREMENT `la_system_notice`
--
ALTER TABLE `la_system_notice`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `la_system_notice_read`
--
ALTER TABLE `la_system_notice_read`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `la_system_resource`
--
ALTER TABLE `la_system_resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;

--
-- 使用表AUTO_INCREMENT `la_system_role`
--
ALTER TABLE `la_system_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键，角色ID', AUTO_INCREMENT=21;

--
-- 使用表AUTO_INCREMENT `la_system_role_dept`
--
ALTER TABLE `la_system_role_dept`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `la_system_role_menu`
--
ALTER TABLE `la_system_role_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- 使用表AUTO_INCREMENT `la_system_storage`
--
ALTER TABLE `la_system_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `la_system_user`
--
ALTER TABLE `la_system_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID', AUTO_INCREMENT=42;

--
-- 使用表AUTO_INCREMENT `la_system_user_group`
--
ALTER TABLE `la_system_user_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- 使用表AUTO_INCREMENT `la_system_user_role`
--
ALTER TABLE `la_system_user_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
