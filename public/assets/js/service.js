
/**
*
* 自定义版 客服咨询js
* @return {[type]} [description]
*/
var flexdClass ='flexdChat';
var rightClose ="rightShow";
var windowType ="wolive-talk"
var rightLine  ="rightminblzxmsgtitlecontainerlabels"
var flexdBgcolor = '#13c9cb';
var doamin='doadmin_url';
var head = document.getElementsByTagName('head')[0];
var link = document.createElement('link');
var url='https://'+doamin;
    link.type='text/css';
    link.rel = 'stylesheet';
    link.href =url+'/assets/css/chatStyle.css';
    head.appendChild(link);

var blzx ={
    visiter_id:(typeof ai_service=='undefined' || typeof ai_service.visiter_id == 'undefined')?'':ai_service.visiter_id,
    visiter_name:(typeof ai_service=='undefined' || typeof ai_service.visiter_name == 'undefined')?'':ai_service.visiter_name,
    avatar:(typeof ai_service=='undefined' || typeof ai_service.avatar == 'undefined')?'':ai_service.avatar,
    product:(typeof ai_service=='undefined' || typeof ai_service.product == 'undefined')?'{}':ai_service.product,
    open:function(){
        var d =document.getElementById('blzxMinChatWindowDiv');
        if(!d){
            var div =document.createElement('div');
            div.id ="blzxMinChatWindowDiv";
            document.body.appendChild(div);
            var w =document.getElementById('blzxMinChatWindowDiv');
            w.classList.add(flexdClass);
            w.innerHTML='<div id="minblzxmsgtitlecontainer"><img id="minblzxWinlogo" src="'+url+'/assets/img/wechatLogo.png"><div id="minblzxmsgtitlecontainerlabel" class="'+rightLine+'" onclick="blzx.connenct(0)">在线咨询</div><img id="minblzxmsgtitlecontainerclosebutton" class="'+rightClose+'" onclick="blzx.closeMinChatWindow(\'blzxMinChatWindowDiv\');" src="'+url+'/assets/img/closewin.png"><img id="minblzxNewBigWin"  class="'+rightClose+'" onclick="blzx.connenct(0)" src="'+url+'/assets/img/up_arrow.png"></div>';
            document.getElementById('minblzxmsgtitlecontainer').style.backgroundColor=flexdBgcolor;
        }
    },
    connenct:function(groupid){
        document.getElementById('blzxMinChatWindowDiv').style.display="none";
        var id =groupid;
        var s =document.getElementById(windowType);
        var web='https://'+doamin+'/admin/#/chat_h5?is_ifram=1';
        if(!s){
            var div = document.createElement('div');
            div.id =windowType;
            div.name=id;
            if(blzx.isMobile()){
               div.style.width='100%';
           }
            document.body.appendChild(div);
            div.innerHTML='<i class="blzx-close" onclick="blzx.closeMinChatWindow(\''+windowType+'\')"></i><iframe id="wolive-iframe" src="'+web+'" onload="pageOk()"></iframe><div id="loading"><img src="/assets/img/loading-2.svg"></div>'
        }else{
            var title =s.name;
            if(title == groupid){
                s.style.display ='block';
            }else{
                s.parentNode.removeChild(s);
                blzx.connenct(groupid); 
            }
        }
    },
    closeMinChatWindow:function(id){
        document.getElementById(id).style.display="none";
        if(id===windowType){
            document.getElementById('blzxMinChatWindowDiv').style.display="block";
        }
    },
    isMobile:function(){
        if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
            return true;
        }else{
            return false;
        }
    }
};

function pageOk(){
    document.getElementById('loading').style.display='none';
}

window.onload =blzx.open();
setTimeout(function () {
    blzx.connenct(0);
},5000);

