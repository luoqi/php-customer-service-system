<?php


namespace app\common\model\server;


use app\common\model\BaseModel;
use think\model;

/**
 * 商品记录管理
 * Class Sentence
 * @package app\common\model\server
 */
class Product extends BaseModel
{
    protected $name = 'kefu_product';
    protected $field = [];
    
    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        
    }
}