<?php


namespace app\common\model\server;


use app\common\model\BaseModel;
use think\model;
use app\common\model\server\Group;
use app\common\model\server\Chats;
/**
 * 用户管理
 * Class Consumer
 * @package app\common\model\server
 */
class Consumer extends BaseModel
    {
        protected $name = 'kefu_user';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        if (!empty($param['name'])) {
            $query->whereLike('name', '%' . trim($param['name']) . '%');
        }
        if (!empty($param['ip'])) {
            $query->where('ip', $param['ip']);
        }
        if (!empty($param['groupid'])) {
            $query->where('group_id', $param['groupid']);
        }
        if(!empty($param['kid']) && $param['kid']>2){
            $query->where('kefu_id', $param['kid']);
        }
    }
    /**
     * @notes 关联消息记录表
     */
    public function msglist(){
        return $this->hasOne('Chats','uid','uid')->order('id', 'desc'); 
    }
    /**
     * @notes 关联分组表
     */
    public function group(){
        return $this->hasOne('Group','id','group_id');
    }
    /**
     * @notes 查询聊天记录列表
     * @param array $where
     * @param string $order
     * @return \think\response\Json
     */
    public function getAll($where=[],$order="id desc"){
        return $this->where($where)->order($order)->select();
    }
}