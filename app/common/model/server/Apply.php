<?php


namespace app\common\model\server;


use app\common\model\BaseModel;
use think\model;


/**
 * 留言管理
 * Class Group
 * @package app\common\model\server
 */
class Apply extends BaseModel
    {
        protected $name = 'kefu_apply';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
       
        if ($username = $param['name'] ?? '') {
            $query->whereLike('name', '%' . trim($username) . '%');
        }
         if ($phone = $param['phone'] ?? '') {
            $query->where('phone', trim($phone));
        }
    }
    /**
     * @notes 查询聊天记录列表
     * @param array $where
     * @param string $order
     * @return \think\response\Json
     */
    public function getAll($where=[],$order="id desc"){
        return $this->where($where)->order($order)->select();
    }
}