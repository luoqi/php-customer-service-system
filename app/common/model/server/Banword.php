<?php


namespace app\common\model\server;


use app\common\model\BaseModel;
use think\model;


/**
 * 违禁词管理
 * Class Banword
 * @package app\common\model\server
 */
class Banword extends BaseModel
    {
        protected $name = 'kefu_banword';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        
    }
    /**
     * @notes 查询违禁词列表
     * @param object $query
     * @param array $param
     * @return \think\response\Json
     */
    public function getAll($where=[],$order="id desc"){
        return $this->where($where)->order($order)->select();
    }
}