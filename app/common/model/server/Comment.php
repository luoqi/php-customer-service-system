<?php


namespace app\common\model\server;


use app\common\model\BaseModel;
use think\model;
use app\common\model\system\SysUser;

/**
 * 评价管理
 * Class Comment
 * @package app\common\model\server
 */
class Comment extends BaseModel
    {
        protected $name = 'kefu_comment';
        protected $field = [];
    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        if (!empty($param['content'])) {
            $query->whereLike('content', '%' . trim($param['content']) . '%');
        }
        if(!empty($param['kefu_id'])){
            $query->where('kefu_id', $param['kefu_id']);
        }
        if(!empty($param['uid'])){
            $query->where('uid', $param['uid']);
        }
        if (!empty($param['time'])) {
            $query->where('created_at','between', [strtotime($param['time'][0]),strtotime($param['time'][1])]);
        }
        if(!empty($param['kid']) && $param['kid']>2){
            $query->where('kefu_id', $param['kid']);
        }
    }
    /**
     * @notes 关联客服表
     */
    public function service(){
        return $this->hasOne(SysUser::class,'id','kefu_id')->field('id,nickname');
    }
    /**
     * @notes 查询聊天记录列表
     * @param array $where
     * @param string $order
     * @return \think\response\Json
     */
    public function getAll($where=[],$order="id desc"){
        return $this->where($where)->order($order)->select();
    }
}