<?php


namespace app\common\model\server;


use app\common\model\BaseModel;
use think\model;


/**
 * 分组管理
 * Class Group
 * @package app\common\model\server
 */
class Group extends BaseModel
    {
        protected $name = 'kefu_group';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        if (!empty($param['type'])) {
            $query->where('type', $param['type']);
        }
    }
    /**
     * @notes 查询聊天记录列表
     * @param array $where
     * @param string $order
     * @return \think\response\Json
     */
    public function getAll($where=[],$order="id desc"){
        return $this->where($where)->order($order)->select();
    }
}