<?php


namespace app\common\model\server;


use app\common\model\BaseModel;
use think\model;
use app\common\model\system\SysUser;

/**
 * 问题回复管理
 * Class Robot
 * @package app\common\model\server
 */
class Robot extends BaseModel
    {
        protected $name = 'kefu_robot';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        if(!empty($param['type'])){
            $query->where('type', $param['type']);
        }
        if(!empty($param['kefu_id'])){
            $query->where('kefu_id', $param['kefu_id']);
        }
        if($param['type']==2 && !empty($param['kid']) && $param['kid']>2){
            $query->where('kefu_id', $param['kid']);
        }
    }
    /**
     * @notes 查询聊天记录列表
     * @param array $where
     * @param string $order
     * @return \think\response\Json
     */
    public function getAll($where=[],$order="id desc"){
        return $this->where($where)->order($order)->select();
    }
    public function service(){
        return $this->hasOne(SysUser::class,'id','kefu_id')->field('id,nickname');
    }
}