<?php
// +----------------------------------------------------------------------
// | likeadmin快速开发前后端分离管理后台（PHP版）
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | gitee下载：https://gitee.com/likeshop_gitee/likeadmin
// | github下载：https://github.com/likeshop-github/likeadmin
// | 访问官网：https://www.likeadmin.cn
// | likeadmin团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeadminTeam
// +----------------------------------------------------------------------

namespace app\common\model;


use think\Model;


/**
 * 基础模型
 * Class BaseModel
 * @package app\common\model
 */
abstract class BaseModel extends Model
{
    protected $autoWriteTimestamp = true;
    protected $createTime = 'created_at';

    protected $updateTime = 'updated_at';
    protected $datetimeFormat = '';

    /**
     * 条件查询
     * @param object $query
     * @param array $param
     */
    abstract function scopeCustom(object $query, array $param);

    /**
     * 分页
     * @param object $query
     * @param array $param
     */
    public function scopePages($query, array $param)
    {
        
        if(!empty($param['page'])) $query->page($param['page'], $param['pageSize']);
    }


}