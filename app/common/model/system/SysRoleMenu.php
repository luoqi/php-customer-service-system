<?php


namespace app\common\model\system;


use app\common\model\BaseModel;
use think\model\Pivot;
use think\model;


/**
 * 系统角色菜单中间件管理
 * Class SysRoleMenu
 * @package app\common\model\system
 */
class SysRoleMenu extends BaseModel
    {
        protected $name = 'system_role_menu';


    /** 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {

    }


    /**
     * 菜单
     */
    public function mate()
    {
        return $this->hasOne(SysMenu::class, 'id', 'menu_id');
    }

   

    }