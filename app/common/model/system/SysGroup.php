<?php

declare(strict_types=1);

namespace app\common\model\system;

use app\common\model\BaseModel;

class SysGroup extends BaseModel
{
    protected $name = 'system_group';

    protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        // 名称
        
    }
}
