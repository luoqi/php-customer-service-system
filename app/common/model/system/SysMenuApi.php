<?php


namespace app\common\model\system;

use app\common\model\BaseModel;
/**
 * 系统菜单管理
 * Class SysMenuApi
 * @package app\common\model\system
 */
class SysMenuApi extends BaseModel
{
    protected $name = 'system_menu_api';
    protected $createTime = 'created_at';

    protected $updateTime = 'updated_at';


    /** 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {

    }


}