<?php

    declare(strict_types=1);

    namespace app\common\model\system;


    use think\model\Pivot;
    class SysUserGroup extends Pivot
    {
        protected $name = 'system_user_group';

        protected $autoWriteTimestamp = true;
        protected $field = [];

        protected $createTime = 'created_at';
        /**
         * 查询条件
         * @param object $query
         * @param array $param
         */
        public function scopeCustom(object $query, array $param)
        {
            // 名称

        }
    }
