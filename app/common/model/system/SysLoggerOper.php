<?php

declare(strict_types=1);

namespace app\common\model\system;

use app\common\model\BaseModel;


class SysLoggerOper extends BaseModel
{
    protected $name = 'system_logger_oper';

    protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        //账户
        if ($username = $param['username'] ?? '') {
            $query->whereLike('username', '%' . trim($username) . '%');
        }
        //地址ip
        if ($ip = $param['ip'] ?? '') {
            $query->whereLike('ip', '%' . trim($ip) . '%');
        }
        //请求方式
        if ($method = $param['method'] ?? '') {
            $query->where('method', trim($method));
        }
        //时间
        if ($time = $param['time_stamp'] ?? '') {
            $timeArr = explode(',', $time);
            $query->whereBetween('time_stamp', [$timeArr[0] / 1000, $timeArr[1] / 1000]);
        }
    }
}
