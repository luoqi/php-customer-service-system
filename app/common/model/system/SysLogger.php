<?php

declare(strict_types=1);

namespace app\common\model\system;

use app\common\model\BaseModel;


class SysLogger extends BaseModel
{
    protected $name = 'system_logger';

    protected $updateTime = false;

    protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        //日志模块
        if ($module = $param['module'] ?? '') {
            $query->where('module', $module);
        }
        //日志等级
        if ($level = $param['level'] ?? '') {
            $query->where('level', $level);
        }
        //日志标签
        if ($tag = $param['tag'] ?? '') {
            $query->whereLike('tag', '%' . trim($tag) . '%');
        }
        //端口
        if ($port = $param['port'] ?? '') {
            $query->where('port', $port);
        }
        //用户ID
        if ($uid = $param['uid'] ?? '') {
            $query->where('uid', $uid);
        }
        //时间区间
        if($time_stamp=$param['time_stamp'] ?? ''){
            $time=explode(',',$time_stamp);
            $query->whereBetweenTime('time_stamp', $time[0]/1000, $time[1]/1000);
        }
    }
    public function user(){
        return $this->hasOne(SysUser::class, 'id', 'uid');;
    }
}
