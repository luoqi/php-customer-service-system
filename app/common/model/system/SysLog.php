<?php


namespace app\common\model\system;


use app\common\model\BaseModel;
use think\model;


/**
 * 系统菜单管理
 * Class SysMenu
 * @package app\common\model\system
 */
class SysLog extends BaseModel
    {
        protected $name = 'system_log';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        // 角色名称
        if ($name = $param['name'] ?? '') {
            $query->whereLike('name', '%' . trim($name) . '%');
        }
        // 角色别名
        if ($alias = $param['alias'] ?? '') {
            $query->whereLike('alias', '%' . trim($alias) . '%');
        }
        // 状态
        $status = $param['status'] ?? '';
        if (is_numeric($status)) {
            $query->where('status', $status);
        }
    }


        public function menus()
        {
            return $this->belongsToMany(SysRole::class, 'system_role_menu','menu_id','role_id');
        }

    }