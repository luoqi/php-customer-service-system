<?php


namespace app\common\model\system;


use app\common\model\BaseModel;
use think\model;


/**
 * 系统菜单管理
 * Class SysMenu
 * @package app\common\model\system
 */
class SysResource extends BaseModel
    {
        protected $name = 'system_resource';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        // 分组
        if(isset($param['group']) && $param['group']!=''){
            if($param['group']=='m'){
                $query->where('group', trim($param['group']));
            }else{
                $query->where('group','<>', trim($param['group']));
            }
            
        }
        //文件名称
        if ($name = $param['name'] ?? '') {
            $query->whereLike('name', '%' . trim($name) . '%');
        }
        //地址ip
        if ($ip = $param['ip'] ?? '') {
            $query->whereLike('ip', '%' . trim($ip) . '%');
        }
        //地址ip
        if ($type = $param['type'] ?? '') {
            $query->whereLike('type', '%' . trim($type) . '%');
        }


    }




    }