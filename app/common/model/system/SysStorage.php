<?php


namespace app\common\model\system;

use app\common\model\BaseModel;
/**
 * 存储设置
 * Class SysStorage
 * @package app\common\model\system
 */
class SysStorage extends BaseModel
{
        protected $name = 'system_storage';
        protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {

    }
    public function getStatusAttr($value,$data)
    {
        return $value ? true:false;
    }

}