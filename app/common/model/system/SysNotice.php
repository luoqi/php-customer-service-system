<?php

declare(strict_types=1);

namespace app\common\model\system;

use app\common\model\BaseModel;


class SysNotice extends BaseModel
{
    protected $name = 'system_notice';

    protected $field = [];

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
    }
}
