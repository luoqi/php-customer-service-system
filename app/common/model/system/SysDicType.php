<?php

    declare(strict_types=1);

    namespace app\common\model\system;

    use app\common\model\BaseModel;

    class SysDicType extends BaseModel
    {
        protected $name = 'system_dic_type';

        protected $field = [];
        protected $createTime = 'created_at';

        protected $updateTime = 'updated_at';

        /**
         * 查询条件
         * @param object $query
         * @param array $param
         */
        public function scopeCustom(object $query, array $param)
        {
            // 名称

        }
    }
