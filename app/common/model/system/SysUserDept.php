<?php

    declare(strict_types=1);

    namespace app\common\model\system;


    use think\model\Pivot;
    class SysUserDept extends Pivot
    {
        protected $name = 'system_user_dept';

        protected $field = [];
        
        /**
         * 查询条件
         * @param object $query
         * @param array $param
         */
        public function scopeCustom(object $query, array $param)
        {
            // 名称

        }
    }
