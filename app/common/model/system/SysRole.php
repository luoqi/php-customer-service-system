<?php


namespace app\common\model\system;


use app\common\model\BaseModel;
use think\model;


/**
 * 系统菜单管理
 * Class SysMenu
 * @package app\common\model\system
 */
class SysRole extends BaseModel
    {
        protected $name = 'system_role';
        protected $field = [];
        protected $createTime = 'created_at';
        protected $updateTime = 'updated_at';

    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        // 角色名称
        if ($name = $param['name'] ?? '') {
            $query->whereLike('name', '%' . trim($name) . '%');
        }
        // 角色名称
        if ($keyword = $param['keyword'] ?? '') {
            $query->whereLike('name', '%' . trim($keyword) . '%');
        }
        // 角色别名
        if ($alias = $param['alias'] ?? '') {
            $query->whereLike('alias', '%' . trim($alias) . '%');
        }
        // 状态
        $status = $param['status'] ?? '';
        if (is_numeric($status)) {
            $query->where('status', $status);
        }
    }


        public function menus()
        {
            return $this->belongsToMany(SysRole::class, 'system_role_menu','menu_id','role_id');
        }

    }