<?php


namespace app\common\model\system;


use app\common\model\BaseModel;
use think\model;


/**
 * 系统菜单管理
 * Class SysMenu
 * @package app\common\model\system
 */
class SysMenu extends BaseModel
    {
        protected $name = 'system_menu';
        protected $createTime = 'created_at';

        protected $updateTime = 'updated_at';








    /** 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {

    }
    public function getAffixAttr($value):bool
    {
        return $value? true:false;
    }
    public function getHiddenBreadcrumbAttr($value, $data):bool
    {
        return $data['hidden_breadcrumb'] ? true : false;
    }
    public function getHiddenAttr($value, $data)
    {
        return $data['hidden'] ? true : false;
    }
    public function getFullpageAttr($value, $data)
    {
        return $value? true:false;
    }
    public function apiList()
    {
        return $this->hasMany(SysMenuApi::class, 'menu_id', 'id');
    }
}