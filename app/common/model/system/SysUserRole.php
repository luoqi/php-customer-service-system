<?php

declare(strict_types=1);

namespace app\common\model\system;

use think\model\Pivot;

/**
 * 用户角色中间件
 * Class SysMenu
 * @package app\common\model\system
 */
class SysUserRole extends Pivot
{
    protected $name = 'system_user_role';
    protected $autoWriteTimestamp = true;
    protected $field = [];
    
    protected $createTime = 'created_at';
    /**
     * 查询条件
     * @param object $query
     * @param array $param
     */
    public function scopeCustom(object $query, array $param)
    {
        // 名称

    }
}
