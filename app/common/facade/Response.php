<?php

declare(strict_types=1);

namespace app\common\facade;

use think\Facade;

    /**
     * 签名验证
     */
    class Response extends Facade
    {
        protected static function getFacadeClass()
        {
            return 'app\common\service\ResponseService';
        }
    }
