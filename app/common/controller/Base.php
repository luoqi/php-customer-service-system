<?php

declare (strict_types=1);

namespace app\common\controller;

use think\facade\App;
use think\Response;
/**
     * 控制器基础类
     */
    abstract class Base
    {
        /**
         * Request实例
         * @var \app\Request
         */
        protected $request;

        /**
         * 应用实例
         * @var \think\App
         */
        protected $app;


        /**
         * 构造方法
         * @access public
         * @param App $app 应用对象
         */
        public function __construct(App $app)
        {
            $this->app = $app;
            $this->request = app('request');
            $this->initialize();
        }


        /**
         * @return mixed
         */
        abstract protected function initialize();


        /**
         * 列表分页参数默认处理
         */
        protected function paging(): array
        {
            $param = $this->request->param();
            $param['page'] = (int) ($param['page'] ?? 1);
            $param['pageSize'] = (int) ($param['pageSize'] ?? 10);
            return $param;
        }


        /**
         * 成功
         * @param array $data 响应数据
         */

        public function success(array|string|int $data = null,$msg = 'success',   int $code = 200): Response
        {
            return self::result($code, $msg, $data);
        }
        /**
         * 错误
         * @param string $message 响应错误信息
         * @param mixed $data 响应数据
         */
        public function error($msg = 'error', $data = [],  int $code = 400): Response
        {
            return self::result($code, $msg, $data);
        }


        public function result(int $code, string $msg= 'success', array|string|int $data = null, int $httpStatus = 200): Response
        {
            $res = compact('code', 'msg', 'data');

            return Response::create($res, 'json', $httpStatus);
        }

    }
