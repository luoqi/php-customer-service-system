<?php


namespace app\common\service;
use think\Response;

class ResponseService
{
    public function success(?array $data = null,$msg = 'success',   int $code = 1): Response
    {
        return self::result($code, $msg, $data);
    }
    /**
     * 错误
     * @param string $message 响应错误信息
     * @param mixed $data 响应数据
     */
    public function error($msg = 'error', $data = [],  int $code = 0): Response
    {
        return self::result($code, $msg, $data,$code);
    }
    
    public function result(int $code, string $msg= 'success', ?array $data = [], int $httpStatus = 200): Response
    {
        $res = compact('code', 'msg', 'data');

        return Response::create($res, 'json', $httpStatus);
    }

}