<?php

declare(strict_types=1);

namespace app\common\service;

use app\common\model\system\SysResource;
use app\common\enum\FileEnum;
use app\common\enum\SysEnum;


/**
 * 文件管理
 */
class FileService
{

    public  function resourceRecord(Array $file): void
    {
        $request = request();
        $resource = new SysResource();
        $resource->group = $request->port ?? SysEnum::PORT_USER;
        $resource->account_id = $request->uid ?? 0;
        $resource->type = FileEnum::classify($file['suffix']);
        $resource->name = $file['fileName'];
        $resource->size = $file['size'] ? round($file['size'] / FileEnum::MB, 3) . 'MB' : '';
        $resource->path = $file['url'];
        $resource->ip = $request->ip() ?? '';
        $resource->save();
    }
}
