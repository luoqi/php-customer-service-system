<?php

declare(strict_types=1);

namespace app\common\exception;

use RuntimeException;

/**
 * 用户端异常 直接弹出错误
 */
class TipsException extends RuntimeException
{

}
