<?php

declare(strict_types=1);

namespace app\common\http\middleware;
use Closure;
use think\Response;

    /**
     * 跨域中间件
     */
    class CrossMiddleware
    {
        /**
         * 处理请求
         * @param Request   $request
         * @param Closure   $next
         * @return \think\Response
         */
        public function handle($request, Closure $next,)
        {
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: X-Requested-With,Content-Type,Token,Authorization,X-Token, User-Agent, Keep-Alive, Origin");
            header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE');
            header('Access-Control-Max-Age: 1800');
            header('Content-Type: application/json; charset=utf-8');
            header('Access-Control-Allow-Credentials:true');
            if (strtoupper($request->method()) == "OPTIONS") {
                return Response::create('ok')->code(200);
            }
            return $next($request);
        }
    }
