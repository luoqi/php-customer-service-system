<?php

declare(strict_types=1);

namespace app\common\enum;

/**
 * 系统公用枚举
 */
class SysEnum
{

    // 管理员登录签名
    const OWN_USIGN = 'sys_user';
    // 会员登录签名
    const OWN_MSIGN = 'member';
    // admin 端口标识
    const PORT_USER = 'A';
    // api 端口标识
    const PORT_MEMBER = 'B';
    // 管理员权限目录
    const ADMIN_MENU_LIST = 'admin_menu_list_';

    // 文件cdn
    const FIEL_CDN = 'tp_file_cdn';
    // 管理登录图片验证码
    const IMG_CAPTCHA = 'captcha_';
    // 操作日志
    const OPERATE_ROLE_API = 'operate_role_api_';
    // 微信小程序access_token
    const WX_ACCESS_TOKEN = 'wx_access_token';
    // 公共密钥
    const COMMON_SECRET = 'D9FOqo6T';
    // 默认密码
    const DEFAULT_PASS = '123456';
    // 限流
    const RATE_LIMITING = 10;

    /**
     * 特殊账号
     * @param integer $uid
     */
    public static function isSpecialAccount(int|array $uid): bool
    {
        $res = false;
        if(is_array($uid)){
            foreach ($uid as $v){
                if(in_array($v, [1, 2])){
                    $res = true;
                    break;
                }
            }
        }else{
            $res =in_array($uid, [1, 2]);
        }
        return $res;
    }

    /**
     * 超管账号
     * @param integer $uid
     */
    public static function isRoot(int $uid)
    {
        return $uid == 1;
    }
    /**
     * 超管角色
     * @param integer $role
     */
    public static function isSuper(int|array|string $role): bool
    {
        if(!is_array($role)){
            $role = json_decode($role); 
        }
       
         return in_array(1, $role)|| in_array(2, $role);
        //return $role == 1;
    }



}
