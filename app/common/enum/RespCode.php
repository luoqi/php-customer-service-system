<?php

declare(strict_types=1);

namespace app\common\enum;
use think\facade\Cache;


/**
 * 状态码
 */
class RespCode
{
    const SYS_ERROR = 500;  // 系统错误

    const SUCCESS   = 200;  // 成功
    const ERROR     = 1;    // 业务错误

    const NOT_LOGIN = 4001; // 未登录
    const UNREGISTERED = 4003; // 未注册
    const ROUTE_ERROR = 4004; // 路由错误

    const DEF_TIPS = '服务异常，请稍等...'; // 服务器错误提示

    /**
     * 操作频繁提示
     */
    public static function often($uid, $tag)
    {
        $index = $uid . '_' . $tag;
        if (Cache::get($index)) {
            tips('操作频繁，请稍后...');
        }
        Cache::set($index, $index, 5);
    }
}
