<?php

declare(strict_types=1);

namespace app\common\enum;

use app\common\service\FileService;
use think\facade\Cache;

class FileEnum
{
    const TEMP = 'temp'; //临时文件
    const AVATAR = 'avatar'; //头像
    const QRCODE = 'qrcode'; //二维码
    const IMAGE = 'image'; //图片
    const VIDEO = 'video'; //视频
    const MP3 = 'mp3'; //mp3,音频
    const FILE = 'file'; //其他文件

    const MB = 1048576; //1MB

    public static function dirtype()
    {
        return [
            self::TEMP, self::AVATAR, self::QRCODE, self::IMAGE,
            self::VIDEO, self::MP3, self::FILE
        ];
    }

    /**
     * 文件cdn
     */
    public static function cdn(): string
    {
        $cdn = Cache::get(CacheMap::FIEL_CDN);
        if (!$cdn) {
            $cdn = (new FileService)->domain;
            Cache::set(CacheMap::FIEL_CDN, $cdn, 10);
        }
        return $cdn;
    }
    /**
     * 文件归类
     * @param string $suffix
     */
    public static function classify(string $suffix): string
    {
        return match ($suffix) {
            '.jpg', '.png', '.bmp', '.jpeg', '.gif' => 'image',
            '.mp4', '.avi', '.mov', '.mpeg' => 'video',
            '.mp3', '.wav' => 'audio',
            default => 'other'
        };
    }
}
