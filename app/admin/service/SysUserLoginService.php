<?php


namespace app\admin\service;

use app\common\enum\SysEnum;
use app\common\facade\JwtAuth;
use app\common\model\system\SysMenuApi;
use think\facade\Cache;
    /**
     * 系统用户登录 服务
     * Class SysUserLoginService
     * @package app\admin\service
     */
    class SysUserLoginService
    {
        /**
         * 生成token
         * @param Object $user
         */
        public function createToken($user): string
        {

            $token = JwtAuth::sign([
                'uid' => $user->id,
                'username' => $user->username,
                'super' => SysEnum::isSuper($user->role_id),
                'role_id' =>is_array($user->role_id)?$user->role_id: json_decode($user->role_id) ,
                'group_id' =>(int) $user->group_id,
            ]);
            Cache::set(SysEnum::OWN_USIGN.$user->id, $token);
            return $token;
        }

        /**
         * @param object $user
         */
        public function auth(object $user)
        {
            return [
                'root' => SysEnum::isSuper($user->role_id),
                'super' =>SysEnum::isRoot($user->id)
            ];
        }

      

    }