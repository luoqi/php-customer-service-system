<?php

declare(strict_types=1);

namespace app\admin\service;

use think\facade\Db;
class LoginLoggerService
{
    private $status;
    /**
     * 登录成功
     * @param string $username
     */
    public function success(string $username): void
    {
        $this->status = 1;
        $this->saveLog($username, '');
    }
    /**
     * 登录失败
     * @param string $username
     * @param string $remark
     */
    public function fail(string $username, $remark = '')
    {
        $this->status = 0;
        $this->saveLog($username, $remark);
    }
    /**
     * 保存日志
     * @param array $log
     */
    private function saveLog($username, $remark)
    {
        $requst = request();
        Db::name('system_logger_login')->insert([
            'username' => $username,
            'ip' => $requst->ip(),
            'os' => self::getOs($requst->header('user-agent')),
            'browser' => self::getBrowser($requst->header('user-agent')),
            'status' => $this->status,
            'message' => $this->status ? '登录成功' : '登录失败',
            'login_time' => date('Y-m-d H:i:s'),
            'remark' => $remark,
            'time_stamp' => time()
        ]);
    }
    /**
     * 获取客户端浏览器
     * @param string $user_agent
     */
    private function getBrowser($user_agent): string
    {
        if (false !== stripos($user_agent, 'MSIE')) {
            $user_browser = 'IE';
        } elseif (false !== stripos($user_agent, 'Firefox')) {
            $user_browser = '火狐';
        } elseif (false !== stripos($user_agent, 'Chrome')) {
            $user_browser = '谷歌';
        } elseif (false !== stripos($user_agent, 'Safari')) {
            $user_browser = '苹果';
        } elseif (false !== stripos($user_agent, 'Opera')) {
            $user_browser = 'Opera';
        } else {
            $user_browser = '其他浏览器';
        }
        return $user_browser;
    }
    /**
     * 获取操作系统
     * @param string $user_agent
     */
    private function getOs($user_agent): string
    {
        if (preg_match('/win/i', $user_agent) && preg_match('/98/i', $user_agent)) {
            $os = 'Windows 98';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/nt 6.0/i', $user_agent)) {
            $os = 'Windows Vista';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/nt 6.1/i', $user_agent)) {
            $os = 'Windows 7';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/nt 6.2/i', $user_agent)) {
            $os = 'Windows 8';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/nt 10.0/i', $user_agent)) {
            $os = 'Windows 10';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/nt 5.1/i', $user_agent)) {
            $os = 'Windows XP';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/nt 5/i', $user_agent)) {
            $os = 'Windows 2000';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/nt/i', $user_agent)) {
            $os = 'Windows NT';
        } else if (preg_match('/win/i', $user_agent) && preg_match('/32/i', $user_agent)) {
            $os = 'Windows 32';
        } else if (preg_match('/linux/i', $user_agent)) {
            $os = 'Linux';
        } else if (preg_match('/unix/i', $user_agent)) {
            $os = 'Unix';
        } else if (preg_match('/sun/i', $user_agent) && preg_match('/os/i', $user_agent)) {
            $os = 'SunOS';
        } else if (preg_match('/ibm/i', $user_agent) && preg_match('/os/i', $user_agent)) {
            $os = 'IBM OS/2';
        } else if (preg_match('/Mac/i', $user_agent)) {
            $os = 'Mac';
        } else if (preg_match('/PowerPC/i', $user_agent)) {
            $os = 'PowerPC';
        } else if (preg_match('/NetBSD/i', $user_agent)) {
            $os = 'NetBSD';
        } else if (preg_match('/FreeBSD/i', $user_agent)) {
            $os = 'FreeBSD';
        } else {
            $os = '未知操作系统';
        }
        return $os;
    }
}
