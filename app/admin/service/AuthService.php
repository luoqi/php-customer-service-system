<?php


namespace app\admin\service;

use app\common\model\system\SysMenuApi;

/**
 * 系统用户登录 服务
 * Class AuthService
 * @package app\admin\service
 */
class AuthService
    {

        /**
         * 角色权限验证
         * @param Request $request
         */
         public function permissionVerify($request)
        {
            $sysMenuId = 0;
            $baseUrl = $request->baseUrl();
            $method = $request->method();
            //admin/system.user/edit
            $baseUrl=  str_replace('/', ':', $baseUrl);
            $baseUrl=  str_replace(':admin:', ':', $baseUrl);
            $api = SysMenuApi::where('api_method', $method)
                ->where('code',$baseUrl )
                ->find();
            if ($api) {
//                if (!SysMenuApi::where('menu_id', $api->menu_id)->find()) {
//                    tips('无操作权限');
//                }
                if ($api->is_record) {//判断是否记录
                    $sysMenuId = $api->menu_id;
                }
            }
            return $sysMenuId;

        }

    }