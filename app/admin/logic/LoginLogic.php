<?php


namespace app\admin\logic;
use app\common\enum\SysEnum;
use app\common\model\system\SysUser;
use think\facade\Cache;

    /**
     * 登录
     * Class LoginLogic
     * @package app\admin\logic
     */
class LoginLogic
{

        /**
         * 获取登录用户
         * @param array $param
         */
        public function login($param):mixed
        {
            $time = time();
            $field = SysUser::showField();
            $sysUser = SysUser::where('username|phone|email', $param['username'])->append(['role_id','group_id'])->with(['roles','role'])->findOrEmpty();

            if ($sysUser->isEmpty()) {
                tips('无账户信息');
            }
            if ($sysUser->status == 0) {
                tips('账户已被禁用');
            }
            if(!empty($param['password'])){
                if ($param['password'] !== $sysUser->password) {
                    tips('账户或密码错误');
                }
            }
            if(!empty($param['code'])){
                if ($param['code'] !== cache($param['username'])) {
                    tips('验证码错误');
                }
            }

            $sysUser->updated_at = $time;
            $sysUser->login_ip = request()->ip();
            $sysUser->save();
            //整合数据
            return $sysUser;
        }

        /**
         * 登出
         */
        public function logout(int $uid): void
        {
            Cache::delete(SysEnum::OWN_USIGN.$uid);

        }
    }