<?php

declare(strict_types=1);

namespace app\admin\logic;


use app\common\service\UploadService;
use app\common\service\FileService;
/**
 * 文件上传
 * Class Upload
 * @package app\admin\controller\system
 */
class UploadLogic
{


    /**
     * 图片文件上传（单文件）
     */
    public static function uploadImage()
    {

        $res = (new UploadService)->image();

        (new FileService)->resourceRecord($res);
       return $res;

    }
    /**
     * 附件文件上传（单文件）
     */
    public static function uploadFile()
    {
        
        $res = (new UploadService)->image('uploads/files');

        (new FileService)->resourceRecord($res);
       return $res;

    }
}
