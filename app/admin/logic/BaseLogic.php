<?php


    namespace app\admin\logic;


    class BaseLogic
    {
        public  object $auth;
        public function __construct()
        {
            $this->auth = request()->auth;
        }

        static public function showTips($model,$id='',$title=''){
            if($id){
                if (!$model->find($id)) {
                    tips($title);
                }
            }
        }

    }