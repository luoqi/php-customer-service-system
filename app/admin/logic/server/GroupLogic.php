<?php

declare(strict_types=1);

namespace app\admin\logic\server;

use app\common\model\server\Group;


/**
 * 分组管理
 */
class GroupLogic
{
    /**
     * @notes 所有列表
     * @param array $param
     * @return \think\response\Json
     */
     public static function getAll(array $param)
    {
        $list = Group::custom($param)
            ->order('id desc')
            ->select();
        return !empty($list) ? $list->toArray() : [];
    }
     /**
     * @notes 编辑分组
     * @param array $param
     * @return bool
     */
    public function edit(array $param)
    {
        $param['id']=$param['value'];
        $param['groupname']=$param['label'];
        return !empty($param['id']) ? Group::update($param) : Group::create($param);
    }
    /**
     * @notes 删除分组
     * @param integer $id
     * @return bool
     */
    public function delete(int $id)
    {
        return Group::where('id', $id)->delete() ? true : false;
    }
}
