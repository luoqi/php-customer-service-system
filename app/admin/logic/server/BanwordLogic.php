<?php

declare(strict_types=1);

namespace app\admin\logic\server;

use app\common\model\server\Banword;
use think\Cache;

/**
 * 违禁词
 */
class BanwordLogic
{
    /**
     * @notes 违禁词列表
     * @param array $param
     * @return \think\response\Json
     */
    public static function lists(array $param)
    {
        $list = Banword::custom($param)
            ->order('id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, Banword::custom($param)->count());
    }
     /**
     * @notes 编辑违禁词
     * @param array $param
     * @param integer $id
     * @return bool
     */
    public function edit(array $param)
    {
        return !empty($param['id']) ? Banword::update($param) : Banword::create($param);
    }
    /**
     * @notes 删除违禁词
     * @param integer $id
     * @return bool
     */
    public function delete(int $id)
    {
        return Banword::destroy($id);
    }
    /**
     * 替换敏感词处理方法
     *
     * 此方法从缓存中获取敏感词列表，如果缓存不存在则查询数据库并设置缓存，
     * 然后遍历敏感词列表，使用空格或其他字符替换文本中的敏感词汇，以达到过滤目的。
     *
     * @param string $content 待处理的文本内容
     * @return string 过滤后的文本内容
     */
    public static function replaceMsg($content)
    {
        $banwords=Banword::cache(600)->column('keyword');
        // 如果有敏感词
        if (!empty($banwords)) {
            // 为每个敏感词准备一个替换符，这里使用'*'代替，实际可根据需求调整
            $replacements = array_fill(0, count($banwords), '*');
            // 使用str_replace批量替换文本中的敏感词
            $content = str_replace($banwords, $replacements, $content);
        }
        // 返回处理后的文本内容
        return $content;
    }

}
