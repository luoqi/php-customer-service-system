<?php

declare(strict_types=1);

namespace app\admin\logic\server;

use app\common\model\server\Apply;


/**
 * 分组管理
 */
class ApplyLogic
{
    /**
     * @notes 所有列表
     * @param array $param
     * @return \think\response\Json
     */
    
    public static function list(array $param)
    {
        $list = Apply::custom($param)
            ->order('id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, Apply::custom($param)->count(),'',$param);
    }
     /**
     * @notes 编辑分组
     * @param array $param
     * @return bool
     */
    public function edit(array $param)
    {
       
        return !empty($param['id']) ? Apply::update($param) : Apply::create($param);
    }
    /**
     * @notes 删除分组
     * @param integer $id
     * @return bool
     */
    public function delete(int $id)
    {
        return Apply::where('id', $id)->delete() ? true : false;
    }
}
