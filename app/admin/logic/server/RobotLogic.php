<?php

declare(strict_types=1);

namespace app\admin\logic\server;

use app\common\model\server\Robot;


/**
 * 问题回复列表
 */
class RobotLogic
{
    /**
     * @notes 问题回复列表
     * @param array $param
     * @return \think\response\Json
     */
    public static function lists(array $param)
    {
        $list = Robot::custom($param)
            ->with('service')
            ->order('id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, Robot::custom($param)->count());
    }
     /**
     * @notes 编辑回复
     * @param array $param
     * @return bool
     */
    public function edit(array $param)
    {
        $param['reply']=strip_tags($param['reply']);
        return !empty($param['id']) ? Robot::update($param) : Robot::create($param);
    }
    /**
     * @notes 删除回复
     * @param integer $id
     * @return bool
     */
    public function delete($id)
    {
        return Robot::whereIn('id',$id)->delete();
    }
}
