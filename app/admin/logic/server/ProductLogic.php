<?php

declare(strict_types=1);

namespace app\admin\logic\server;

use app\common\model\server\Product;

/**
 * 商品记录
 */
class ProductLogic
{
    /**
     * @notes 商品记录
     * @param array $param
     * @return \think\response\Json
     */
    public static function list(array $param)
    {
        $list = Product::custom($param)
            ->order('id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, Product::custom($param)->count());
    }
}
