<?php

declare(strict_types=1);

namespace app\admin\logic\server;

use app\common\model\server\Setting;
use app\common\model\system\SysStorage;
use app\api\controller\Event;

/**
 * 设置列表
 */
class SettingLogic
{
    /**
     * @notes 设置列表
     * @param array $param
     * @return \think\response\Json
     */
    public static function lists(array $param)
    {
        $list = Setting::custom($param)
            ->order('id desc')
            ->pages($param)
            ->select();
            
        //获取当前当前文件的存储路径
        
       
        return list_fmt($list, Setting::custom($param)->count());
    }
     /**
     * @notes 编辑设置
     * @param array $param
     * @return bool
     */
    public function edit(array $param,$domain)
    {
        $param['is_off']= $param['is_off']==true ? 1 : 2;
         $param['worktime']=!empty($param['start']) && !empty($param['end']) ?  $param['start'].'-'.$param['end'] : '';
        if( $param['domain'] == ''){
            $param['domain'] = $domain;
             $domainArr = explode("//",$domain);
              if($domainArr[0] == 'https'){
                  $param['ws_url']   = 'wss://'. $domainArr[1].'wss';
              }else{
                  $param['ws_url']   = 'ws://'. $domainArr[1].':2345';
              }
        }
        $settingMOdel = new Setting;
        try{
            if(!empty($param['id'])){ //id为空表示新增
                $settingMOdel = $settingMOdel->find($param['id']);
            }
            $changFixedType = false;
            $changeWindowType = false;
            if(isset($param['is_float'])){
                if( $param['is_float'] ==2){
                    $changFixedType = true;
                }
                if($param['is_window'] ==2){
                    $changeWindowType = true;
                }
                $this->createMinDiyjs($param,$changFixedType,$changeWindowType);
            }
            $settingMOdel->save($param);
            return $settingMOdel;
           
        } catch (Exception $e) {
            tips('设置失败');
        }

    }
    public function editconfig(array $params){
        // 获取当前配置详情并转换为数组
        $currentConfig = self::detail();
        // 初始化短信配置，若存在则解码，否则使用默认配置
        $smsConfig = !empty($currentConfig['sms_config']) ? json_decode($currentConfig['sms_config'], true) : config('sms');
        // 更新指定类型的短信配置项，仅当参数中有对应键且值不为空时覆盖原有值
        $smsTypeConfig = &$smsConfig[$params['type']];
        array_walk($smsTypeConfig, function (&$value, $key) use ($params) {
            if (array_key_exists($key, $params) && !empty($params[$key])) {
                $value = $params[$key];
            }
        });
        // 将更新后的配置回写到数据库
        $updateData = [
            'id' => $currentConfig['id'],
            'sms_config' => json_encode($smsConfig),
            'sms_default' => $params['sms_default']
        ];
        // 执行更新操作并返回更新结果
        return Setting::update($updateData);
    }
    public function editemail(array $params){
        // 将更新后的配置回写到数据库
        $updateData = [
            'id' => 1,
            'email_config' => json_encode($params),
            'is_email'=>$params['is_email']
        ];
        // 执行更新操作并返回更新结果
        return Setting::update($updateData);
    }
     /**
     * 获取设置详情
     *
     * @param array $where 查询条件，默认为空，即获取第一条记录
     * @return \think\Model|null 设置信息或null
     */
    public static function detail(array $where = [])
    {
        $storageType = SysStorage::where('status',1)->find();
        $setting = Setting::where('id',1)->find()->toArray();
        $setting['file_path'] =!empty($storageType['domain']) ? $storageType['domain'] : 'default';
        return $setting;
    }

    /**
     * 客服设置检查及通知
     *
     * @param int $uid 用户ID
     * @return mixed 设置信息或发送给用户的离线消息
     */
    public static function checkAndNotify($uid)
    {
        // 获取默认设置
        $settings = self::detail(['id' => 1]);
        // 如果客服完全关闭
        if ($settings['is_off'] == 2) {
            // 发送通知给用户，告知无客服在线
            return Event::sendUid($uid, ['type' => 'no_server', 'msg' => '没有客服在线，请留言']);
        }
        // 解析工作时间
        $workTime = explode('-', str_replace(':', '', $settings['worktime']));
        // 检查当前时间是否在工作时间内
        if (self::isTimeInRange($workTime)) {
            // 如果不在工作时间，发送通知
            return Event::sendUid($uid, ['type' => 'no_server', 'msg' => '不在工作时间，请留言']);
        }

        // 返回设置信息，表示一切正常
        return $settings;
    }

    /**
     * 判断当前时间是否在指定的工作时间段内
     *
     * @param array $time 工作时间段，两个元素的数组，格式为 [开始时间, 结束时间]
     * @return bool 当前时间是否在工作时间段外
     */
    protected static function isTimeInRange(array $time)
    {
        // 跨夜班情况
        $isNightShift = $time[0] > $time[1];
        // 当前时间转换为24小时制的秒数
        $currentSeconds = strtotime(date('H:i:s'));
        // 检查时间范围
        if ($isNightShift) {
            // 如果是跨夜班，判断时间不在两个时间段内
            return !($currentSeconds >= strtotime($time[0]) || $currentSeconds <= strtotime($time[1]));
        } else {
            // 非跨夜班，直接判断是否在时间段外
            return $time[0] < $time[1] && ($currentSeconds < strtotime($time[0]) || $currentSeconds > strtotime($time[1]));
        }
    }
    
      /**
     *
     * [自定义弹窗脚本 description]
     * @return [type] [description]
     */
    public function createMinDiyjs($param, $changFixed, $changeWindow)
    {
        $http_type = ((isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $url = $http_type . $_SERVER['HTTP_HOST'];
        
        $js = @file_get_contents(app()->getRootPath(). "/public/assets/js/service.js");//读取JS文件内容
        $js=str_replace('doadmin_url',$_SERVER['HTTP_HOST'],$js);//替换域名
        if(!is_null($param['float_color'])){
             $js=str_replace('#13c9cb',$param['float_color'],$js);//替换域名
        }
       
        //悬浮在右侧是修改
        if($changFixed){
            $js=str_replace('flexdChat','blzxMinChatWindowDivs',$js);
            $js=str_replace('rightminblzxmsgtitlecontainerlabels','minblzxmsgtitlecontainerlabels',$js);
            $js=str_replace('rightShow','hidden',$js);
           
        }
        //大窗口是修改 窗口大小
         if($changeWindow){
            $js=str_replace('wolive-talk','wolive-talk-big',$js);
        }
        
        $file =file_put_contents(app()->getRootPath() ."/public/assets/js/service_1.js", $js);//覆盖JS文件
        $path = $url. "/assets/js/service.js";
        $data = ['code' => 0, 'msg' => '生成js成功!', 'data' => $path];
        return $data;

    }
}
