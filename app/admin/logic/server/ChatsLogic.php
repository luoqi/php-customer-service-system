<?php

declare(strict_types=1);

namespace app\admin\logic\server;

use app\common\model\server\Chats;
use app\api\controller\Event;
use app\admin\logic\server\BanwordLogic;
use app\admin\logic\server\ConsumerLogic;
use app\common\model\system\SysUser;
/**
 * 聊天记录
 */
class ChatsLogic 
{
    /**
     * @notes 聊天记录列表
     * @param array $param
     * @return \think\response\Json
     */
    public static function lists(array $param)
    {
        $list = Chats::custom($param)
            ->with(['user','kefu'])
            ->order('id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, Chats::custom($param)->count());
    }
    /**
     * 获取用户聊天历史记录
     *
     * @param int $uid 用户ID
     * @return array
     */
    public static function getMsgList($uid){
        return self::getAll(['uid'=>$uid]);
    }
     /**
     * 撤销消息
     *
     * @param array $data 操作数据
     * @return void
     */
    public static function cancel($data){
        Chats::where('unstr', $data['unstr'])->delete();
        Event::sendUid($data['uid'], ['type'=>'cancel','msg'=>'成功','data'=>$data['unstr']]);
    }
    /**
     * 发送消息并处理后续逻辑
     *
     * @param array $data 消息数据，含内容、方向、用户ID、客服ID等
     * @return void
     */
    public static function sendMessage(array $data): void
    {
        // 过滤并清理消息内容中的敏感词与空白字符
        $data['content'] = BanwordLogic::replaceMsg(trim($data['content']));
        
        // 保存消息记录
        Chats::create($data);
        
        // 定义接收消息的用户ID
        $receiverId = $data['direction'] === 1 ? $data['kefu_id'] : $data['uid'];

        // 获取接收方信息
        if($data['uid']) $userInfo = ConsumerLogic::detail(['uid' => $data['uid']]);

        // 准备发送的数据包，包含时间戳和用户信息
        $data['created_at']=time();
        $sendMessagePayload = [
            'user' => $userInfo,
            'msg' => '成功',
            'data' => $data
        ];
        
        // 发送消息通知
        Event::sendUid($receiverId, array_merge(['type' => 'msg'], $sendMessagePayload));
        
        // 转接客服处理
        if ($data['direction'] === 3) {
            // 获取被转接客服信息
            $serverInfo = SysUser::where('id', $data['switch_id'])->field('id, nickname, avatar')->find();
            if ($serverInfo) {
                // 通知用户转接的客服信息
                Event::sendUid($data['uid'], [
                    'type' => 'serverdetail',
                    'msg' => '成功',
                    'data' => $serverInfo
                ]);
                // 更新转接客服的在线用户列表
                ConsumerLogic::sendOnlineList($data['switch_id']);
            }
        }
            
            
        if (empty($data['kefu_id'])) {
            // 未指定客服，加入排队列表
            ConsumerLogic::addPdList($data);
        }
    }
     /**
     * 删除单条聊天记录
     *
     * @param int $id 聊天记录ID
     * @return bool
     */
    public function delete($id)
    {
        return Chats::whereIn('id',$id)->delete();
    }
    public static function edit(array $param)
    {
        return !empty($param['id']) ? Chats::update($param) : Chats::create($param);
    }
    /**
     * 获取所有聊天记录
     *
     * @param array $where 查询条件
     * @return array
     */
    public static function getAll($where){
        $data= Chats::with('kefu')->where($where)->order('id asc')->select();
        return !empty($data) ? $data->toArray() : [];
    }
}
