<?php

declare(strict_types=1);

namespace app\admin\logic\system;

use app\common\model\system\SysLog;


/**
 * 管理员分组
 */
class SysLogLogic
{
    /**
     * 列表
     * @param array $param
     */
    public static function lists(array $param)
    {
        $list = SysLog::custom($param)
            ->order('sort desc')
            ->select();
        return recursion($list->toArray(),0,false,'parent_id');
    }
    /**
     * 删除
     * @param integer $id
     */
    public function delete(int $id)
    {
        if ($id == 1) {
            tips('无权限删除');
        }
        if (SysUser::where('group_id', $id)->find()) {
            tips('分组在使用中，不可删除');
        }
        return SysDept::where('id', $id)->delete() ? true : false;
    }
}
