<?php


namespace app\admin\logic\system;
use app\admin\logic\BaseLogic;
use app\common\model\system\SysRole;
use app\common\model\system\SysRoleDept;
use app\common\model\system\SysRoleMenu;

/**
 * 角色
 */
class SysRoleLogic extends BaseLogic
{

    public  object $auth;

    public function __construct()
    {
        $this->auth = request()->auth;
    }

    /**
     * 列表
     * @param array $param
     */
     static public function lists(array $param)
    {

        $list = SysRole::custom($param)
            ->order('sort asc')
            ->pages($param)
            ->select();
        return list_fmt($list, SysRole::custom($param)->count(),'',$param);
    }
    /**
     * 编辑
     * @param array $data 角色信息
     * @param integer $id 角色id
     */
    public function edit(array $data)
    {
        // 角色别名查重
        $this->_aliasRepeat($data['alias'], $data['id']);
        return !empty($data['id']) ? SysRole::update($data) : SysRole::create($data);
    }
    /**
     * 角色别名查重
     * @param string $alias 角色别名
     * @param integer $id 角色id
     */
    protected function _aliasRepeat($alias,$id)
    {
        $map = SysRole::where('alias', $alias);
        if ($id) {
            $map->where('id', '<>', $id);
        }
        if ($map->find()) {
            tips('角色别名重复，请重新设置');
        }
    }

    /**
     * 设置角色权限
     * @param array $data
     * @param integer $id 角色id
     */
    public function permission(array $data, int $id)
    {

        $roleMenuModel = new SysRoleMenu;
        $role = SysRole::find($id);
        if (!$role) {
            tips('未找到角色信息，权限设置失败');
        }
        $role->data_scope = $data['data_scope'];
        $role->save();
        // 菜单
        $oldMenu = $roleMenuModel->where('role_id', $id)->select()->toArray();
        if ($oldMenu) {
            $oldMenu = array_column($oldMenu, null, 'menu_id');
        }
        $newMenu = [];
        foreach ($data['menu'] as $menu_id) {
            if (isset($oldMenu[$menu_id])) {
                unset($oldMenu[$menu_id]);
            } else {
                $newMenu[] = ['role_id' => $role->id, 'menu_id' => $menu_id];
            }
        }
        if ($newMenu) {
            $roleMenuModel->saveAll($newMenu);
        }
        if ($oldMenu) {
            $roleMenuModel->whereIn('id', array_column($oldMenu, 'id'))->delete();
        }
    }
    /**
     * 角色菜单
     * @param integer $id 角色id
     */

    public static function roleMenuId(int $id)
    {
        return SysRoleMenu::where('role_id', $id)->column('menu_id');
    }

    /**
     * 角色权限验证
     * @param Request $request
     */
    public static function permissionVerify($request): array
    {
        $baseUrl = $request->baseUrl();
        $method = $request->method();
        $api = SysMenuApi::where('api_method', $method)
            ->where('code', str_replace('/', ':', $baseUrl))
            ->find();
        if (!$api) {
            return [];
        }
        $roleMenu = SysRoleMenu::where('menu_id', $api->menu_id)
            ->where('role_id', $request->auth->role_id)
            ->find();
        if (!$roleMenu) {
            tips('无操作权限');
        }
        //记录操作信息
        if ($api->oper_log) {
            $param = $request->param();
            return [
                'username' => $request->auth->username,
                'method' => $method,
                'router' => $baseUrl,
                'service_name' => $api->menu_id,
                'ip' => $request->ip(),
                'ip_location' => '',
                'request_data' => is_array($param) ? json_encode($param) : '',
            ];
        }
    }

    /**
     * 删除菜单
     * @param array $ids
     */
    public function delete(array $ids): void
    {
        if (!$ids) {
            tips('无角色信息');
        }
        //删除角色 关联的菜单以及 部门
        SysRole::destroy($ids);
        SysRoleMenu::where('role_id','in',$ids);
        SysRoleDept::where('role_id','in',$ids);

    }


}