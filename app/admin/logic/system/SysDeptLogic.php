<?php

declare(strict_types=1);

namespace app\admin\logic\system;

use app\common\model\system\SysDept;
use app\common\model\system\SystemUser;
use app\admin\service\auth\UserService;

/**
 * 管理员分组
 */
class SysDeptLogic
{
    /**
     * 列表
     * @param array $param
     */
    public static function lists(array $param)
    {
        $list = SysDept::custom($param)
            ->order('sort desc')
            ->select();
            
        $list=recursion($list->toArray(),0,false,'parent_id');
        return $list;
    }
    /**
     * 编辑
     * @param array $data
     * @param integer $id
     */
    public function edit(array $data)
    {
        $this->_nameRepeat($data);
        return !empty($data['id']) ? SysDept::update($data) : SysDept::create($data);
    }
    /**
     * 名称查重
     * @param string $name
     */
    protected function _nameRepeat($data)
    {
        $where = SysDept::where('name', $data['label']);
        if (!empty($data['id'])) {
            $where->where('id', '<>', $data['id']);
        }
        if ($where->find()) {
            tips('【' . $data['label'] . '】已被使用');
        }
    }
    /**
     * 删除
     * @param integer $id
     */
    public function delete(int $id)
    {
        if ($id == 1) {
            tips('无权限删除');
        }
        if (SysUser::where('group_id', $id)->find()) {
            tips('分组在使用中，不可删除');
        }
        return SysDept::where('id', $id)->delete() ? true : false;
    }
}
