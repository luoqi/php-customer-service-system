<?php

declare(strict_types=1);

namespace app\admin\logic\system;

use app\common\model\system\SysLogger;
use app\common\model\system\SysLoggerLogin;
use app\common\model\system\SysMenu;
use app\common\model\system\SysLoggerOper;
class SysLoggerLogic
{
    /**
     * 登录日志列表
     * @param array $param
     */
    public function loginList(array $param): array
    {
        $list = SysLoggerLogin::custom($param)
            ->order('id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, SysLoggerLogin::custom($param)->count());
    }
    /**
     * 操作日志列表
     * @param array $param
     */
    public function operList(array $param): array
    {
        $list = SysLoggerOper::custom($param)
            ->field('id,username,method,router,service_name,ip,response_code,created_at')
            ->order('id desc')
            ->pages($param)
            ->select();

        return list_fmt($list, SysLoggerOper::custom($param)->count());
    }
    /**
     * 操作日志详情
     * @param integer $id
     */
    public function operInfo(int $id)
    {
        $info = SysLoggerOper::field('id,request_data,response_code,response_data')->findOrEmpty($id);
        if (!$info->isEmpty()) {
            $info = $info->toArray();
            if ($info['request_data']) {
                $info['request_data'] = json_decode($info['request_data'], true);
            }
            if ($info['response_data']) {
                $info['response_data'] = json_decode($info['response_data'], true);
            }
        }
        return $info;
    }
    /**
     * 操作日志记录
     * @param array $operLog
     * @param mixed $code 响应状态码
     * @param mixed $content 响应数据
     */
    public static function operRecord(array $record, $code, $content): void
    {
        $record['response_code'] = $code;
        $record['response_data'] = $content;
        //业务模块
        $service = SysMenu::alias('c')->field('c.title ct,p.title pt')
            ->where('c.id', $record['service_name'])
            ->leftJoin('o_system_menu p', 'p.id = c.parent_id')
            ->find();
        $record['service_name'] = '【' . $service->pt . '】' . $service->ct;
        $record['created_at'] = now_date();
        $record['time_stamp'] = time();
        SysLoggerOper::insert($record);
    }
    /**
     * 系统日志列表
     * @param array $param
     */
    public function systemList(array $param): array
    {
        $list = SysLogger::custom($param)
            ->with('user')
            ->order('id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, SysLogger::custom($param)->count());
    }


}
