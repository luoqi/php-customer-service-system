<?php

declare(strict_types=1);

namespace app\admin\logic\system;

use app\admin\logic\BaseLogic;
use app\common\model\system\SysGroup;
use app\common\model\system\SystemUser;
use app\admin\service\auth\UserService;
use app\common\model\system\SysUser;


/**
 * 管理员分组
 */
class SysGroupLogic extends BaseLogic
{
    /**
     * 列表
     * @param array $param
     */
    public static function lists(array $param)
    {
        $list = SysGroup::custom($param)
            ->order('sort desc')
            ->pages($param)
            ->select();
        return recursion($list->toArray(),0,false,'parent_id');
    }
    /**
     * 编辑
     * @param array $data
     * @param integer $id
     */
    public function edit(array $data)
    {
        return !empty($data['id']) ? SysGroup::update($data) : SysGroup::create($data);
    }
    /**
     * 名称查重
     * @param string $name
     * @param integer $id
     */
    protected function _nameRepeat($name, $id = 0)
    {
        $model=new SysGroup;
        $where = $model->where('label', $name);
        if ($id) {
            $where->where('id', '<>', $id);
        }
        self::showTips($model,$id,'【' . $name . '】已被使用');
        
    }
    /**
     * 删除
     * @param integer $id
     */
    public function delete(int $id)
    {
        if ($id == 1) {
            tips('无权限删除');
        }
        if (SysUser::where('group_id', $id)->find()) {
            tips('分组在使用中，不可删除');
        }
        return SysGroup::where('id', $id)->delete() ? true : false;
    }
}
