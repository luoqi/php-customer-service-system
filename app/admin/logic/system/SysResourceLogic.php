<?php

declare(strict_types=1);

namespace app\admin\logic\system;

use app\common\model\system\SysResource as  ResourceModel;

/**
 * 文件资源
 */
class SysResourceLogic
{
    /**
     * 文件类型
     */
    public function typeList()
    {
        return ResourceModel::typeList();
    }
    /**
     * 列表
     * @param array $param
     */
    public function list(array $param)
    {
        $list = ResourceModel::custom($param)
            ->pages($param)
            ->order('id desc')
            ->select();
        return list_fmt($list, ResourceModel::custom($param)->count());
    }
    /**
     * 编辑
     * @param array $param
     */
    public function edit($param)
    {
        $model = ResourceModel::find($param['id']);
        if (!$model) {
            tips('无文件信息，修改失败');
        }
        $model->notes = $param['notes'];
        $model->deletable = $param['deletable'];
        $model->save();
    }
    /**
     * 删除
     * @param array $ids
     */
    public function del($ids)
    {
        return ResourceModel::whereIn('id', $ids)->delete();
    }
}
