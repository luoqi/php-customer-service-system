<?php

declare(strict_types=1);
namespace app\admin\logic\system;

use app\admin\logic\BaseLogic;
use app\common\model\system\SysDic as   DictDataModel;
use app\common\model\system\SysDicType as  DictTypeModel;
use app\admin\service\auth\UserService;
/**
 * 逻辑层--字典管理
 * Class SysDicLogic
 * @package app\adminapi\logic\system
 */
class SysDicLogic extends BaseLogic
{
    //后台用户登录信息
    public  object $auth;

    public function __construct()
    {
        $this->auth = request()->auth;
    }
    /**
     * 字典类型列表
     * @param array $param
     */
    public function typeList(array $param)
    {
        $list = DictTypeModel::custom($param)
            ->order('sort desc, id desc')
            ->select()->toArray();
        return recursion($list, 0, false, 'parent_id');
    }
    /**
     * 字典类型编辑
     * @param array $data
     * @param integer $id
     */
    public function typeEdit(array $data, int $id): void
    {
        $auth =  $this->auth;
        $dictType = new DictTypeModel();
        if ($id) {
            $dictType = $dictType->find($id);
            if (!$dictType) {
                tips('无字典类型信息，修改失败');
            }
            $data['updated_by'] = $auth->username;
        }else {
            $data['created_by'] = $auth->username;
        }
       $data['parent_id'] =  $data['parent_id']??0;
        $this->_dictTypeRepeat($data->code, $id);
        if ($dictType->save($data)) {
            tips('保存失败');
        }
    }
    /**
     * 字典类型code查重
     * @param string $code 字典标示
     * @param integer $id
     */
    private function _dictTypeRepeat(string $code, int $id): void
    {
        $where = DictTypeModel::where('code', $code);
        if ($id) {
            $where->where('id', '<>', $id);
        }
        if ($where->find()) {
            tips('字典标识【' . $code . '】已被使用');
        }
    }
    /**
     * 字典类型删除
     * @param integer $id
     */
    public function typeDelete(int $id): void
    {
        $dictType = DictTypeModel::where('id', $id)->find();
        if (!$dictType) {
            tips('未找到相关');
        }
        DictTypeModel::where('id', $dictType->id)->delete();
        DictDataModel::where('type_id', $dictType->id)->delete();
        $this->_typeDeleteChildren((int) $dictType->id);
    }
    /**
     * 字典类型子级删除
     * @param integer $parent_id
     */
    private function _typeDeleteChildren(int $parent_id): void
    {
        $typeList = DictTypeModel::where('parent_id', $parent_id)->select();
        if ($typeList->isEmpty()) {
            return;
        }
        foreach ($typeList as $dictType) {
            DictTypeModel::where('id', $dictType->id)->delete();
            DictDataModel::where('type_id', $dictType->id)->delete();
            $this->_typeDeleteChildren((int) $dictType->id);
        }
    }
    /**
     * 字典数据列表
     * @param array $param
     */
    public function dataList(array $param)
    {
        $list = DictDataModel::custom($param)
            ->order('sort asc,id desc')
            ->pages($param)
            ->select();
        return list_fmt($list, DictDataModel::custom($param)->count());
    }
    /**
     * 字典数据编辑
     * @param array $data
     * @param integer $id
     */
    public function dataEdit(array $data, int $id): void
    {
        $auth =  $this->auth;
        $dictData = new DictDataModel();
        if ($id) {
            $dictData = $dictData->find($id);
            if (!$dictData) {
                tips('无字典类型信息，修改失败');
            }
            $data['updated_by']= $auth->username;
        } else {
            $data['created_by']= $auth->username;
        }
        $dictType = DictTypeModel::find($dictData->type_id);
        if (!$dictType) {
            tips('未找到字典类型');
        }
        $this->_dictDataRepeat($dictData->label, $id);
        if (!$dictData->save($data)) {
            tips('保存失败');
        }
    }
    /**
     * 字典数据label查重
     * @param string $label 键名
     * @param integer $id
     */
    private function _dictDataRepeat(string $label, int $id): void
    {
        $where = DictDataModel::where('label', $label);
        if ($id) {
            $where->where('id', '<>', $id);
        }
        if ($where->find()) {
            tips('键名【' . $label . '】已被使用');
        }
    }
    /**
     * 字典数据删除
     * @param array $ids
     */
    public function dataDelete(array $ids)
    {
        return DictDataModel::whereIn('id', $ids)->delete();
    }
}
