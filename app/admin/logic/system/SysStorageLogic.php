<?php

declare(strict_types=1);
namespace app\admin\logic\system;

use app\admin\logic\BaseLogic;

use app\common\model\system\SysStorage;
use app\admin\service\auth\UserService;
use think\facade\Cache;

/**
 *存储管理
 * Class SysDicLogic
 * @package app\adminapi\logic\system
 */
class SysStorageLogic extends BaseLogic
{
    public  object $auth;

    public function __construct()
    {
        $this->auth = request()->auth;
    }


    /**
     * 列表
     * @param array $param
     */
    public static function list(array $param)
    {
        $list = SysStorage::custom($param)
            ->order('id asc')
            ->pages($param)
            ->select();
        return $list->toArray();
    }
    /**
     * 编辑
     * @param array $data
     * @param integer $id
     */
    public function edit(array $data)
    {
      //获取 设置的存储缓存
        $storageType = Cache::get('storage_type_tag')??'';//初始化本地储存
        if(!$storageType){
            $storageType = SysStorage::where('status',1)->find()['tags'];
        }
        $storage = SysStorage::find($data['id']);
        $data['status'] = $data['status']?1:0;
        if($data['status']){
            if($storageType!=$storage['tags']){
                SysStorage::where('tags',$storageType)->update(['status'=>0]);
                $storageType = $storage['tags'];
            }
        }else{
            if($storage['tags'] != 'local'){//关闭是需要设置本地未启动状态
                $storageType = 'local';
                SysStorage::where('tags','local')->update(['status'=>1]);
            }
        }

        Cache::set('storage_type_tag',$storageType);
        return  $storage->save($data) ? true : false;
    }


}
