<?php
// +----------------------------------------------------------------------
// | 路由设置
// +----------------------------------------------------------------------
return [
    'middleware'    =>    [
         //跨域中间件
        app\common\http\middleware\CrossMiddleware::class,
        // 初始中间件
        app\admin\http\middleware\InitMiddleware::class,
        // 登录中间件
        app\admin\http\middleware\SysUserLoginMiddleware::class,
        //权限验证
        app\admin\http\middleware\AuthMiddleware::class,
    ]
];
