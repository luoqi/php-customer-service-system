<?php

declare(strict_types=1);

namespace app\admin\http\middleware;

use app\admin\service\AuthService;
use app\admin\service\OperLoggerService;
use app\common\facade\Response as ResponseFacade;
use think\Response;

    /**
     * 服务端权限中间件
     */
    class AuthMiddleware
    {
        /**
         * 处理请求信息
         * @param Request $request
         * @param string $token
         * @param Closure  $next
         * @return Response
         */

        public function handle($request,$next)
        {
            $isNotLogin =  $request->isNotLogin;
            if($isNotLogin){
                return $next($request);
            }

            if($request->uid == 0) {
                return  ResponseFacade::error('请求缺少参数token_auth',[],0);
            }

           //  是否是超级管理员
//            if (1 === $request->auth->super) {
//                return $next($request);
//            }
            //权限认证
            $sysMenuId=(new AuthService)->permissionVerify($request);
            $request->sysMenuId = $sysMenuId;
         
            return $next($request);
        }

    }
