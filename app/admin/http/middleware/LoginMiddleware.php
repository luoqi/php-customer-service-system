<?php

declare (strict_types=1);
namespace app\admin\http\middleware;
use Closure;
use think\Response;

class LoginMiddleware
    {
        /**
         * 登录请求请求验证
         * @param Request   $request
         * @param Closure   $next
         * @return \think\Response
         */
        public function handle($request, Closure $next,)
        {


            return $next($request);
        }
    }