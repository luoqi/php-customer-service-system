<?php
namespace app\admin\validate;

use app\common\validate\BaseValidate;
/**
 * 用户表验证
 * Class UserValidate
 * @package app\admin\validate
 */
class UserValidate extends BaseValidate
{
    protected $rule = [
        'id'         => 'integer|egt:0',
        'nickname'   => 'require|length:2,20',
        'username'   => 'require|length:2,20',
        'status'     => 'integer|in:0,1',
        'sort'       => 'integer|egt:1|elt:255',
        'data_scope' =>'integer|in:1,2,3,4',
    ];

    protected $message = [
        'nickname.require' => '请输入姓名',
        'username.require' => '请输入密码',
        'id.integer'       => 'ID 应为数字',
        'id.egt'           => 'ID 大于0',

    ];

    /**
     * @notes  密码验证
     * @param $password
     * @param $data
     */
    public function checkPassword($password, $data)
    {
        // 登录限制
    }
    protected $scene = [
        'edit'         =>  ['id','nickname','username'],
        'permission'   =>  ['id','data_scope'],
        'del'          =>  ['id']
    ];
//    public function sceneEdit()
//    {
//        return $this->only(['id','nickname','username']);
//    }

}