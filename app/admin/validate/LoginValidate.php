<?php
namespace app\admin\validate;

use app\common\validate\BaseValidate;
/**
 * 登录验证
 * Class LoginValidate
 * @package app\adminapi\validate
 */
class LoginValidate extends BaseValidate
{
    protected $rule = [
        'username'  => 'require|max:25|min:5',
        'password' => 'require',
    ];

    protected $message = [
        'username.require'  => '请输入账号',
        'username.max'      => '名称最多不能超过25个字符',
        'username.min'      => '名称最少不能小于6个字符',
        'password.require' => '请输入密码'
    ];

    /**
     * @notes  密码验证
     * @param $password
     * @param $data
     */
    public function checkPassword($password, $data)
    {
        // 登录限制
    }


    // Test 验证场景定义
    public function sceneTest()
    {
        return $this->only(['username'])
            ->remove('username','min:6')
            ->append('username', 'require|min:3');

    }

}