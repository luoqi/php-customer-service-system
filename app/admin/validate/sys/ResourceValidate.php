<?php

namespace app\admin\validate\sys;

use app\common\validate\BaseValidate;
/**
 * 菜单表验证
 * Class ResourceValidate
 * @package app\admin\validate\sys
 */
class ResourceValidate extends BaseValidate
{
    protected $rule = [
        'id'        => 'integer|egt:0',
        'name'      => 'require|length:2,20',
        'ids'       => 'array'
    ];


    protected $scene = [
        'edit'      =>  ['id','name'],
        'delete'    =>['ids'],

    ];
    }