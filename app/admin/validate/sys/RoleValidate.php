<?php

namespace app\admin\validate\sys;

use app\common\validate\BaseValidate;
/**
 * 角色表验证
 * Class RoleValidate
 * @package app\admin\validate\sys
 */
class RoleValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'integer|egt:0',
        'name' => 'require|length:2,20',
        'alias' => 'require|length:2,20',
        'status' => 'integer|in:0,1',
        'sort' => 'integer|egt:1|elt:255',
        'ids'=>"array",
        'data_scope' => 'integer|in:1,2,3,4',
    ];

    protected $message = [
        'name.require' => '请输入姓名',
        'alias.require' => '请输入别名',
        'id.integer' => 'ID 应为数字',
        'id.egt' => 'ID 大于0',

    ];
    protected $scene = [
        'edit'  =>  ['id','name','alias'],
        'del'  =>['ids'],
        'permission'=>['id','data_scope']
    ];

}