<?php

namespace app\admin\validate\sys;

use app\common\validate\BaseValidate;
/**
 * 菜单表验证
 * Class RoleValidate
 * @package app\admin\validate\sys
 */
class MenuValidate extends BaseValidate
{
    protected $rule = [
        'id'        => 'integer|egt:0',
        'parent_id' => 'integer|egt:0',
        'name'      => 'require|length:2,20',
        'type'      => 'integer|in:menu,button,link,iframe',
        'sort'      => 'integer|egt:1|elt:255',
        'apiList'   => 'array',
        'ids'       => 'array'
    ];


    protected $scene = [
        'edit'      =>  ['id','name','alias'],
        'delete'    =>['ids'],

    ];
    }