<?php

namespace app\admin\validate\sys;

use app\common\validate\BaseValidate;
/**
 * 部门表验证
 * Class UserGroupValidate
 * @package app\admin\validate
 */
class UserGroupValidate extends BaseValidate
{

    protected $rule = [
        'id'        => 'integer|egt:0',
        'label'     => 'require|length:2,20',
        'status'    => 'integer|in:0,1',
        'sort'      => 'integer|egt:1|elt:255',
    ];

    protected $message = [
        'label.require' => '请输入姓名',
        'id.integer'    => 'ID 应为数字',
        'id.egt'        => 'ID 大于0',

    ];

    protected $scene = [
        'edit'  =>  ['id','label'],
        'del'   =>['id']
    ];

}