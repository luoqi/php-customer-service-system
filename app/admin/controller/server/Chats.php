<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\ChatsLogic;
/**
 * 聊天记录管理
 * Class Chats
 * @package app\admin\controller\server
 */
class Chats extends BaseAdmin
{
    protected $chats;

    public function initialize()
    {
        $this->chats = new ChatsLogic;
    }
    /**
    * @notes 查看聊天记录列表
    * @return  \think\response\Json
    */
    public function lists()
    {
        return $this->success(ChatsLogic::lists(array_merge($this->paging(),['kid'=>$this->request->auth->uid])));
    }
    /**
     * @notes 删除聊天记录
     * @return  \think\response\Json
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->chats->delete($data['ids']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
