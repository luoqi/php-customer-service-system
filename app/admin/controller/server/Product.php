<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\ProductLogic;
/**
 * 商品咨询记录管理
 * Class Product
 * @package app\admin\controller\server
 */
class Product extends BaseAdmin
{
    protected $sentence;

    public function initialize()
    {
        $this->product = new ProductLogic;
    }
    /**
    * @notes 查看记录
    * @return  \think\response\Json
    */
    public function list()
    {
        return $this->success(ProductLogic::list($this->paging()));
    }
}
