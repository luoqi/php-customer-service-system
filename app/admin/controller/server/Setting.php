<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\SettingLogic;

/**
 * 设置管理
 * Class Group
 * @package app\admin\controller\system
 */
class Setting extends BaseAdmin
{
    protected $setting;

    public function initialize()
    {
        $this->setting = new SettingLogic;
    }
    /**
     * @notes 查看设置列表
     * @return  \think\response\Json
     */
    public function lists()
    {
  
        return $this->success(SettingLogic::lists($this->paging()));
    }
    /**
     * @notes 编辑设置
     * @return  \think\response\Json
     */
    public function edit()
    {
        $param = $this->request->post();
      
        $domain = $this->request->domain(); // 获取当前域名
        $res = $this->setting->edit($param,$domain);
        return $res ? $this->success($res->toArray()) : $this->error('保存失败');
    }
    //配置短信
    public function smsconfig(){
        $param = $this->request->post();
        $res = $this->setting->editconfig($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
    //配置邮箱
    public function editemail(){
        $param = $this->request->post();
        $res = $this->setting->editemail($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
}
