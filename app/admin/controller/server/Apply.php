<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\ApplyLogic;
/**
 * 分组管理
 * Class Group
 * @package app\admin\controller\server
 */
class Apply extends BaseAdmin
{
    protected $apply;

    public function initialize()
    {
        $this->apply = new ApplyLogic;
    }
    /**
     * @notes 查看所有分组列表
     * @return  \think\response\Json
     */
    public function list()
    {
        return $this->success(ApplyLogic::list($this->paging()));
    }
    /**
     * @notes 编辑分组
     * @return  \think\response\Json
     */
    public function edit()
    {
        $param = $this->request->post();
        $res = $this->apply->edit($param);
        return $res ? $this->success($res->id) : $this->error('保存失败');
    }
    /**
     * @notes 删除分组
     * @return  \think\response\Json
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->apply->delete($data['id']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
