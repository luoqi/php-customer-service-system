<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\RobotLogic;
/**
 * 问题回复管理
 * Class Robot
 * @package app\admin\controller\server
 */
class Robot extends BaseAdmin
{
    protected $robot;

    public function initialize()
    {
        $this->robot = new RobotLogic;
    }
    /**
     * @notes 查看回复列表
     * @return  \think\response\Json
     */
    public function lists()
    {
         return $this->success(RobotLogic::lists(array_merge($this->paging(),['kid'=>$this->request->auth->uid])));
    }
    /**
     * @notes 查看所有回复列表
     * @return  \think\response\Json
     */
    public function getreply(){
        return $this->success(RobotLogic::lists(['kefu_id'=>$this->paging()['kefu_id'],'type'=>2]));
    }
    /**
     * @notes 编辑回复
     * @return  \think\response\Json
     */
    public function edit()
    {
        $param = $this->request->post();
        $param=array_merge($param,['kefu_id'=>$this->request->auth->uid]);
        $res = $this->robot->edit($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
    /**
     * @notes 删除回复
     * @return  \think\response\Json
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->robot->delete($data['ids']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
