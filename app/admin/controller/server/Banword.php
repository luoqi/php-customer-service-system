<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\BanwordLogic;
/**
 * 违禁词管理
 * Class Banword
 * @package app\admin\controller\server
 */
class Banword extends BaseAdmin
{
    protected $banword;

    public function initialize()
    {
        $this->banword = new BanwordLogic;
    }
    /**
     * @notes 查看违禁词列表
     * @return  \think\response\Json
     */
    public function lists()
    {
        return $this->success(BanwordLogic::lists($this->paging()));
    }
    /**
     * @notes 编辑违禁词
     * @return  \think\response\Json
     */
    public function edit()
    {
        $param = $this->request->post();
        $res = $this->banword->edit($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
    /**
     * @notes 删除违禁词
     * @return  \think\response\Json
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->banword->delete($data['id']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
