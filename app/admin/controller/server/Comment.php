<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\CommentLogic;
/**
 * 评价管理
 * Class Comment
 * @package app\admin\controller\server
 */
class Comment extends BaseAdmin
{
    protected $comment;

    public function initialize()
    {
        $this->comment = new CommentLogic;
    }
    /**
     * @notes 查看评论列表
     * @return  \think\response\Json
     */
    public function lists()
    {
         return $this->success(CommentLogic::lists(array_merge($this->paging(),['kid'=>$this->request->auth->uid])));
    }
    /**
     *  @notes 编辑评论
     *  @return  \think\response\Json
     */
    public function edit()
    {
        $param = $this->request->post();
        $res = $this->comment->edit($param, (int) $param['id']);
        return $res ? $this->success() : $this->error('保存失败');
    }
    /**
     * @notes 删除评论
     * @return  \think\response\Json
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->comment->delete($data['ids']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
