<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\SentenceLogic;
/**
 * 问候语管理
 * Class Sentence
 * @package app\admin\controller\server
 */
class Sentence extends BaseAdmin
{
    protected $sentence;

    public function initialize()
    {
        $this->sentence = new SentenceLogic;
    }
    /**
    * @notes 查看问候语列表
    * @return  \think\response\Json
    */
    public function lists()
    {
        return $this->success(SentenceLogic::lists(array_merge($this->paging(),['kid'=>$this->request->auth->uid])));
    }
    /**
     * @notes 编辑问候语
     * @return  \think\response\Json
     */
    public function edit()
    {
        $param = $this->request->post();
        $res = $this->sentence->edit($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
    /**
     * @notes 删除问候语
     * @return  \think\response\Json
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->sentence->delete($data['ids']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
