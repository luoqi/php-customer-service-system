<?php

declare(strict_types=1);

namespace app\admin\controller\server;

use app\admin\controller\BaseAdmin;
use app\admin\logic\server\ConsumerLogic;
/**
 * 客户管理
 * Class Consumer
 * @package app\admin\controller\server
 */
class Consumer extends BaseAdmin
{
    protected $consumer;
    public function initialize()
    {
        $this->consumer = new ConsumerLogic;
    }
    /**
     * @notes 查看客户列表
     * @return  \think\response\Json
     */
    public function list()
    {
        return $this->success(ConsumerLogic::list(array_merge($this->paging(),['kid'=>$this->request->auth->uid])));
    }
    /**
     * @notes 查看客户列表
     * @return  \think\response\Json
     */
    public function getOnline(){
        $param = $this->request->post();
        $res = $this->consumer->getOnline($param);
        return $this->success($res);
    }
    /**
     * @notes 认领客户
     * @return  \think\response\Json
     */
    public function claim(){
        $param = $this->request->post();
        $res = $this->consumer->claim($param);
        return $this->success('认领成功');
    }
    /**
     * @notes 编辑客户
     * @return  \think\response\Json
     */
    public function edit()
    {
        $param = $this->request->post();
        $res = $this->consumer->edit($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
    public function census(){
        $param = $this->request->post();
        $res = $this->consumer->census(array_merge($param,['kid'=>$this->request->auth->uid]));
        return $res ? $this->success($res) : $this->error();
    }
    /**
     * @notes 删除客户
     * @return  \think\response\Json
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->consumer->delete($data['id']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
