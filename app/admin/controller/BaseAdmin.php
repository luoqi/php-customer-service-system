<?php

declare (strict_types=1);

namespace app\admin\controller;

use app\common\controller\Base;
use think\Validate;
/**
 * 控制器基础类
 * Class BaseAdmin
 * @package app\admin\controller
 *
 */
class BaseAdmin extends Base
{
    /**
     * 当前登陆管理员信息
     */
    protected object|array $adminInfo=[];
    /**
     * 当前登陆管理员ID
     */
    protected int $adminId = 0;
    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;
    /**
     * 初始化
     */
    public function initialize()
    {
        if (isset($this->request->auth) && $this->request->auth) {
            $this->adminInfo = $this->request->auth;
            $this->adminId = $this->request->uid;
        }
    }

    /**
     * 验证数据
     * @param  array        $data     数据
     * @param  array        $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    final protected function validate(array $data, array $validate, array $message = [], bool $batch = false)
    {
        $v = new Validate();
        $v->rule($validate);
        $v->message($message);
        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }
        return $v->failException(true)->check($data);
    }



}
