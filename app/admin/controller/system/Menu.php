<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysMenuLogic;
use app\admin\validate\sys\MenuValidate;
use app\common\exception\TransException;
/**
 * 菜单栏
 * Class Menu
 * @package app\admin\controller\system
 */
class Menu extends BaseAdmin
{

    /**
     * 管理员菜单
     */
    public function lists()
    {
        return $this->success(SysMenuLogic::lists($this->paging()));
    }
    /**
     * 管理员菜单
     */
    public function userMenu()
    {
        $result['menu'] = SysMenuLogic::getMenuByAdminId($this->adminInfo);
        //树形结构
        $result['permissions'] = SysMenuLogic::roleMenuApi($this->request->auth->role_id, 'code');
        return $this->success($result);
    }

    /**
     * 菜单列表
     */
    public function list()
    {
        $param = $this->request->all();
        return $this->success($this->menuService->list($param));
    }
    /**
     * 菜单编辑
     */
    public function edit()
    {
        $param = $this->request->all();
        validate(MenuValidate::class)->scene('edit')->check($param);
        return TransException::exec(SysMenuLogic::class, 'edit', $param, (int) $param['id']);
    }
    /**
     * 菜单删除
     */
    public function delete()
    {
        $param = $this->request->all();
        validate(MenuValidate::class)->scene('delete')->check($param);
        return TransException::exec(SysMenuLogic::class, 'delete', $param['ids']);
    }
}
