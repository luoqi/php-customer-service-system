<?php

    declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysRoleLogic;
use app\admin\logic\system\SysUserLogic;
use app\admin\validate\UserValidate;
use app\common\exception\TransException;


/**
 * 系统用户数据
 * Class User
 * @package app\admin\controller\system
 */
class User extends BaseAdmin
    {

    /**
         * 用户列表
         */
        public function lists()
        {
            return $this->success(SysUserLogic::lists(array_merge($this->paging(),['kid'=>$this->request->auth->uid])));
        }
        /**
         * 角色编辑
         */
        public function edit()
        {
            $param = $this->request->post();
//            $this->validate($param, [
//                'id' => 'integer|egt:0',
//                'nickname' => 'require|length:2,20',
//                'alias' => 'require|length:2,20',
//                'status' => 'integer|in:0,1',
//                'sort' => 'integer|egt:1|elt:255',
//            ]);
            validate(UserValidate::class)->scene('scene_edit')->check($param);

            return TransException::exec(SysUserLogic::class, 'edit', $param);
        }
        /**
         * 角色权限
         */
        public function permission()
        {
            $data = $this->request->post();
            $this->validate($data, [
                'id' => 'integer|egt:0',
                'data_scope' => 'integer|in:1,2,3,4',
            ]);
            return TransException::exec(SysUserLogic::class, 'permission', $data, (int) $data['id']);
        }
        /**
         * 角色权限菜单id
         */
        public function roleMenu()
        {
            $param = $this->request->get();
            $res['menu'] = SysRoleLogic::roleMenuId((int) $param['id']);
            return $this->success($res);
        }
    /**
     * 修改个人信息
     */
    public function modifyUser()
    {
        $param = $this->request->put();
        validate(UserValidate::class)->scene('scene_edit')->check($param);

        return TransException::exec(SysUserLogic::class, 'modifyUser', $param);

    }
    /**
     * @notes 删除回复
     * @return  \think\response\Json
     */
    public function delete()
    {
        $data = $this->request->post();
        $del = (new SysUserLogic)->delete($data['ids']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
