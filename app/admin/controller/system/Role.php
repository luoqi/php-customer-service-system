<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysRoleLogic;
use app\admin\validate\sys\RoleValidate;
use app\common\exception\TransException;
/**
 * 角色
 * Class Role
 * @package app\admin\controller\system
 */
class Role extends BaseAdmin
{
    /**
     * 角色列表
     */
    public function lists()
    {
        return $this->success(SysRoleLogic::lists($this->paging()));
    }
    /**
     * 角色编辑
     */
    public function edit()
    {
        $param = $this->request->post();
        validate(RoleValidate::class)->scene('edit')->check($param);

        return TransException::exec(SysRoleLogic::class, 'edit', $param, (int) $param['id']);
    }
    /**
     * 角色权限
     */
    public function permission()
    {
        $param = $this->request->post();
        validate(RoleValidate::class)->scene('permission')->check($param);
        $res = $this->robot->edit($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
    /**
     * 角色权限菜单id
     */
    public function roleMenu()
    {
        $param = $this->request->get();
        $res['menu'] = SysRoleLogic::roleMenuId((int) $param['id']);
        return $this->success($res);
    }

    /**
     * 菜单删除
     */
    public function delete()
    {
        $param = $this->request->all();
        validate(RoleValidate::class)->scene('del')->check($param);
        return TransException::exec(SysRoleLogic::class, 'delete', $param['ids']);
    }

    }
