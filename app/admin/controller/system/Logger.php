<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysLoggerLogic;
/**
 * 系统日志
 * Class Logger
 * @package app\admin\controller\system
 */
class Logger extends BaseAdmin
{
    protected $sysLoggerLogic;

    public function initialize()
    {
        $this->sysLoggerLogic = new SysLoggerLogic;
    }




    /**
     * 登录日志列表
     */
    public function loginList()
    {
        return $this->success($this->sysLoggerLogic->loginList($this->paging()));
    }
    /**
     * 操作日志列表
     */
    public function operList()
    {
        return $this->success($this->sysLoggerLogic->operList($this->paging()));
    }
    /**
     * 操作日志详情
     */
    public function operInfo()
    {
        $param = $this->request->get();
        return $this->success($this->sysLoggerLogic->operInfo((int) $param['id']));
    }
    /**
     * 操作日志 - 当前用户
     */
    public function userOper()
    {
        $param = $this->paging();
        $param['username'] = $this->request->auth->username;
        return $this->success($this->sysLoggerLogic->operList($param));
    }
    /**
     * 系统日志列表
     */
    public function systemList()
    {
        return $this->success($this->sysLoggerLogic->systemList($this->paging()));
    }
}
