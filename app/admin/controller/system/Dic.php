<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysDicLogic;
use app\common\exception\TransException;
/**
 * 系统配置--字典管理
 * Class Dic
 * @package app\admin\controller\system
 */
class Dic extends BaseAdmin
{

    protected $sysDicLogic;

    public function initialize()
    {
        $this->sysDicLogic = new SysDicLogic;
    }

    /**
     * 字典类型列表
     */
    public function typeList()
    {
        return $this->success($this->sysDicLogic->typeList($this->request->get()));
    }
    /**
     * 字典类型编辑
     */
    public function typeEdit()
    {
        $data = $this->request->post();
        return $this->success($this->sysDicLogic->typeEdit($data, (int) $data['id']));
    }
    /**
     * 字典类型删除
     */
    public function typeDelete()
    {
        $param = $this->request->post();
        return TransException::exec(sysDicLogic::class, 'typeDelete', (int) $param['id']);
    }
    /**
     * 字典数据列表
     */
    public function dataList()
    {
        return $this->success($this->sysDicLogic->dataList($this->paging()));
    }
    /**
     * 字典数据编辑
     */
    public function dataEdit()
    {
        $data = $this->request->post();
        return $this->success($this->sysDicLogic->dataEdit($data, (int) $data['id']));
    }
    /**
     * 字典数据删除
     */
    public function dataDelete()
    {
        $param = $this->request->post();
        $res = $this->sysDicLogic->dataDelete([$param['id']]);
        return $res ? $this->success() : $this->error();
    }
    /**
     * 字典数据批量删除
     */
    public function dataDeletes()
    {
        $param = $this->request->post();
        $res = $this->sysDicLogic->dataDelete($param['ids']);
        return $res ? $this->success() : $this->error();
    }
}
