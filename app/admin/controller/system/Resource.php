<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysResourceLogic;
use app\common\exception\TransException;
/**
 * 文件资源
 * Class Resource
 * @package app\admin\controller\system
 */
class Resource extends BaseAdmin
{
    protected $sysResourceLogic;

    public function initialize()
    {
        $this->sysResourceLogic = new SysResourceLogic;
    }
    /**
     * 文件类型
     */
    public function typeList()
    {
        return $this->success($this->sysResourceLogic->typeList());
    }
    /**
     * 列表
     */
    public function lists()
    {
        return $this->success($this->sysResourceLogic->list($this->paging()));
    }
    /**
     * 编辑
     */
    public function edit()
    {
        $param = $this->request->post();
        return $this->success($this->sysResourceLogic->edit($param));
    }
    /**
     * 删除
     */
    public function del()
    {
        $param = $this->request->post();
        $this->validate($param, []);
        $del = $this->sysResourceLogic->del($param['ids']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
