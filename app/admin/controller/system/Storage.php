<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysStorageLogic;

/**
 *存储设置
 * Class Group
 * @package app\admin\controller\system
 */
class Storage extends BaseAdmin
{
    protected $sysStorageLogic;

    public function initialize()
    {
        $this->sysStorageLogic = new SysStorageLogic;
    }
    /**
     * 列表
     */
    public function list()
    {
        return $this->success($this->sysStorageLogic->list($this->paging()));
    }
    /**
     * 编辑
     */
    public function edit()
    {
        $param = $this->request->post();

        $res = $this->sysStorageLogic->edit($param);
        return $res ? $this->success() : $this->error('保存失败');
    }

}
