<?php

declare(strict_types=1);

namespace app\admin\controller\system;

use app\admin\controller\BaseAdmin;
use app\admin\logic\system\SysGroupLogic;
use app\admin\validate\sys\UserGroupValidate;
/**
 * 管理员分组--部门管理
 * Class Group
 * @package app\admin\controller\system
 */
class Group extends BaseAdmin
{
    protected $sysGroupLogic;

    public function initialize()
    {
        $this->sysGroupLogic = new SysGroupLogic;
    }
    /**
     * 列表
     */
    public function lists()
    {
        return $this->success(SysGroupLogic::lists($this->paging()));
    }
    /**
     * @notes 编辑
     * @return array
     */
    public function edit()
    {
        $param = $this->request->post();
        validate(UserGroupValidate::class)->scene('edit')->check($param);
        $res = $this->sysGroupLogic->edit($param);
        return $res ? $this->success() : $this->error('保存失败');
    }
    /**
     * @notes 删除
     * @return array
     */
    public function del()
    {
        $data = $this->request->post();
        $del = $this->sysGroupLogic->delete($data['id']);
        return $del ? $this->success() : $this->error('删除失败');
    }
}
