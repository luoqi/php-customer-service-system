<?php

declare(strict_types=1);

namespace app\admin\listener;


use app\common\model\system\SysMenu;
use app\common\model\system\SysLoggerOper;
class SysLoggerListener
{
    /**
     *  管理员操作日志
     * @param $response
     */
    public function handle($response)
    {

        $request = request();
        $sysMenuId=(int) $request->sysMenuId;
        if( $request->sysMenuId>0){
            $param = $request->param();
            $record['response_code'] = $response->getCode();
            $record['response_data'] = $response->getContent();

            // 业务模块
            $service = SysMenu::alias('c')
                ->field('c.title ct,p.title pt')
                ->leftJoin('la_system_menu p', 'p.id = c.parent_id')
                ->where('c.id', $sysMenuId)
                ->find();
            $record['service_name'] ='【' . $service->pt . '】' . $service->ct;;
            $record['username'] = $request->auth->username;
            $record['ip'] = $request->ip();
            $record['method'] =  $request->method();
            $record['router'] = $request->baseUrl();
            $record['request_data'] = is_array($param) ? json_encode($param) : '';
            $record['created_at'] = date('Y-m-d H:i:s');
            $record['time_stamp'] = time();
            SysLoggerOper::insert($record);
        }
    }


}
