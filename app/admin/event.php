<?php
return [
    'listen' => [
        'AppInit' => [],
        'HttpRun' => [],
        'HttpEnd' => ['app\admin\listener\SysLoggerListener'],
        'LogLevel' => [],
        'LogWrite' => [],
    ]
];