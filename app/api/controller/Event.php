<?php

namespace app\api\controller;

use think\Db;
use think\Log;
use think\Request;
use think\Session;
use app\common\model\server\Business;
use GatewayWorker\Lib\Gateway;
use app\api\controller\Server;
use app\admin\logic\server\ConsumerLogic;
use app\admin\logic\system\SysUserLogic;
use app\admin\logic\server\ChatsLogic;
use app\admin\logic\server\CommentLogic;

// 允许跨域请求
header('Access-Control-Allow-Origin:*');

/**
 * 跨域公用事件处理控制器
 */
class Event
{
    /**
     * 给指定UID的客户端发送消息
     * @param int $uid 用户ID
     * @param array $message 消息内容
     */
    public static function sendUid($uid, $message){
        Gateway::sendToUid($uid, json_encode($message));
    }
    public static function sendAll($message){
        Gateway::sendToAll(json_encode($message));
    }
    /**
     * 向指定客服分组发送消息
     * @param string $group 分组名称
     * @param array $message 消息内容
     */
    public static function sendGroup($group, $message){
        Gateway::sendToGroup($group, json_encode($message));
    }

    /**
     * 客户端加入指定群组
     * @param string $client_id 客户端ID
     * @param string $group 群组名称
     */
    public static function addGroup($client_id, $group){
        Gateway::joinGroup($client_id, $group);
    }

    /**
     * 客户端离开指定群组
     * @param string $client_id 客户端ID
     * @param string $group 群组名称
     */
    public static function offGroup($client_id, $group){
        Gateway::leaveGroup($client_id, $group);
    }

    /**
     * 绑定客户端ID与UID
     * @param string $client_id 客户端ID
     * @param int $uid 用户ID
     */
    public static function addUid($client_id, $uid){
        Gateway::bindUid($client_id, $uid);
    }

    /**
     * 当客户端连接时处理
     * @param string $client_id 客户端ID
     */
    public static function onConnect($client_id){
        ConsumerLogic::createUid($client_id);
    }

    /**
     * 向特定客户端发送消息
     * @param string $client_id 客户端ID
     * @param array $message 消息内容
     */
    public static function sendClient($client_id, $message){
        Gateway::sendToClient($client_id, json_encode($message));
    }

    /**
     * 判断指定UID的用户是否在线
     * @param int $uid 用户ID
     * @return bool 是否在线
     */
    public static function isuidonline($uid){
        return Gateway::isUidOnline($uid);
    }

    /**
     * 根据UID获取客户端ID
     * @param int $uid 用户ID
     * @return string|null 客户端ID 或 null（未找到）
     */
    public static function getCidByUid($uid){
        return Gateway::getClientIdByUid($uid);
    }

    /**
     * 接收客户端消息时触发的方法
     * @param string $client_id 客户端ID
     * @param mixed $message 接收到的消息
     */
    public static function onMessage($client_id, $message)
    {
        // 打印接收到消息的相关日志
        var_dump([
            'client' => "{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']}",
            'gateway' => "{$_SERVER['GATEWAY_ADDR']}:{$_SERVER['GATEWAY_PORT']}",
            'client_id' => $client_id,
            'session' => json_encode($_SESSION),
            'onMessage' => $message
        ]);

        $data = json_decode($message, true);
        if (empty($data['type'])) return;

        switch ($data['type']) {
            case 'ping':
	            Gateway::sendToClient($client_id, json_encode(['type'=>'pong']));
	            break;
            case 'login': //访客登录
                ConsumerLogic::home($client_id,$data);
                break;
            case 'serverlogin': //客服登录
                SysUserLogic::serverLogin($client_id,$data);
                break;
            case 'select': //选择客服
                ConsumerLogic::selectServer($data);
                break;
            case 'sendserver': //发送消息
                ChatsLogic::sendMessage($data);
                break;
            case 'black': //加入删除黑名单
                ConsumerLogic::blackList($data);
                break;
            case 'sendvalue': //给客服提前发送消息
                SysUserLogic::sendValue($data);
                break;
            case 'sendpdlist': //给所有客服推送排队列表
                ConsumerLogic::addPdList($data);
                break;
            case 'endchat': //是否结束对话
                Gateway::sendToUid($data['uid'], json_encode($data));
                break;
            case 'comment': //给客服进行评价
                CommentLogic::comment($data);
                break;
            case 'jxchat': //客户选择继续聊天通知客服
                Gateway::sendToUid($data['kefu_id'], json_encode($data));
                break;
            case 'cancel': //撤销消息
                ChatsLogic::cancel($data);
                break;
        }
    }

    /**
     * 客户端关闭连接时处理
     * @param string $client_id 客户端ID
     */
    public static function onClose($client_id){
        ConsumerLogic::editOnline($client_id);
    }
}