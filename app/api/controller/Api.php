<?php
// +----------------------------------------------------------------------
// | YuanAuth [ YuanAuth 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.yfxw.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 源分享并不是自由软件，未经许可不能去掉源分享相关版权
// +----------------------------------------------------------------------
// | Author: Kacin Team <26341012@qq.com>
// +----------------------------------------------------------------------
namespace app\api\controller;
use think\Db;
use think\Request;
use app\common\controller\Base;
use SingKa\Sms\SkSms;
use think\facade\Config;
/**
 * 授权验证接口
 */
class Api extends Base
{
    protected $noNeedLogin = ['getSetting'];
    protected $noNeedRight = ['getSetting'];
    public function initialize(){
        
    }
    /**
    * 短信发送示例
    *
    * @mobile  短信发送对象手机号码
    * @action  短信发送场景，会自动传入短信模板
    * @parme   短信内容数组
    */
    public function sendSms($mobile, $action, $parme)
    {
        //$this->SmsDefaultDriver是从数据库中读取的短信默认驱动
        $SmsDefaultDriver = $this->SmsDefaultDriver ?: 'aliyun'; 
        //$this->SmsConfig是从数据库中读取的短信配置
        $config = $this->SmsConfig ?: Config::get('sms.'.$SmsDefaultDriver);
        $sms = new sksms($SmsDefaultDriver, $config);//传入短信驱动和配置信息
        //判断短信发送驱动，非阿里云和七牛云，需将内容数组主键序号化
        if ($this->SmsDefaultDriver == 'aliyun') {
            $result = $sms->$action($mobile, $parme);
        } elseif ($this->SmsDefaultDriver == 'qiniu') {
            $result = $sms->$action([$mobile], $parme);
        } elseif ($this->SmsDefaultDriver == 'smsbao') {
            $result = $sms->$action($mobile, $parme);
        } else {
            $result = $sms->$action($mobile, $this->restoreArray($parme));
        }
        if ($result['code'] == 200) {
            $data['code'] = 200;
            $data['msg'] = '短信发送成功';
        } else {
            $data['code'] = $result['code'];
            $data['msg'] = $result['msg'];
        }
        return $data;
    }
  	
    /**
    * 数组主键序号化
    *
    * @arr  需要转换的数组
    */
    public function restoreArray($arr)
    {
        if (!is_array($arr)){
            return $arr;
        }
        $c = 0;
        $new = [];
        foreach ($arr as $key => $value) {
            $new[$c] = $value;
            $c++;
        }
        return $new;
    }
    public function ceshi(){

        $config=Config::get('email');

        var_dump($config);die;
        $date='2024-5-11';
        $data=Product::whereTime('created_at', '>=', strtotime($date))->select();
        $productIdList=array_column($data->toArray(),'product_id');
        $arrayValue=array_count_values($productIdList);
        $currentNum=Consumer::where(['kefu_id'=>2,'is_online'=>1])->count();
        $queueNum=Consumer::where(['kefu_id'=>NULL])->count();
        $onlineServerNum=SysUser::where(['state'=>1])->count();
        $currentAllNum=Consumer::where(['kefu_id'=>2])->count();
        $chatNumToday=Chats::where(['kefu_id'=>2])->whereTime('created_at', '>=', 'today')->count();
        $chatAllNum=Chats::where(['kefu_id'=>2])->count();
        $commentNumToday=Comment::where(['kefu_id'=>2])->whereTime('created_at', '>=', 'today')->count();
        $commentAllNum=Comment::where(['kefu_id'=>2])->count();
        var_dump($currentNum);
        var_dump($queueNum);
        var_dump($onlineServerNum);
        var_dump($currentAllNum);
        var_dump($chatNumToday);
        var_dump($chatAllNum);
        var_dump($commentNumToday);
        var_dump($commentAllNum);
        var_dump($arrayValue);
    }
    public function getSetting(){
        return $this->success('成功');
    }
}
