<?php
// +----------------------------------------------------------------------
// | YuanAuth [ YuanAuth 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2021 https://www.yfxw.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 源分享并不是自由软件，未经许可不能去掉源分享相关版权
// +----------------------------------------------------------------------
// | Author: Kacin Team <26341012@qq.com>
// +----------------------------------------------------------------------
namespace app\api\controller;
use think\Db;
use think\Config;
use think\Request;

/**
 * 授权验证接口
 */
class Update
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    
    
    public function index()
    {
        $version = $this->request->request('version');
        $authkey = $this->request->request('syskey');
        $codeauth =  db('auth_sys')->where('syskey','=',$authkey)->find();
        if(empty($codeauth)){
            $res['code']=0;
            $res['msg'] = '该程序不存在';
            return json($res);
        }
        if($version==$codeauth['sysversion']){
            $res['code']=0;
            $res['msg'] = '已是最新版本';
            return json($res);
        }
        //检查版本
        $new = db('auth_update')->where('authsys_id',$codeauth['id'])->where('banben',$version)->find();
        if(empty($new)){
            //查不到更新版本,为初始版本
            $news = db('auth_update')->where('authsys_id',$codeauth['id'])->order('id asc')->limit(1)->select();
        }else{
           $news = db('auth_update')->where('authsys_id',$codeauth['id'])->where('banben_code','gt',$new['banben_code'])->order(['id asc'])->limit(1)->select(); 
        }
        $request = \think\Request::instance();
        $res['code']=100;
        $res['msg'] = '请求成功';
        $res['url'] =$request->root(true). $news[0]['update_file'];
        return json($res);
    }
 
    
}
