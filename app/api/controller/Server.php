<?php

namespace app\api\controller;

use think\Db;
use think\Log;
use think\Request;
use think\facade\Session;
use app\common\model\server\Consumer;
use app\common\model\server\Group;
use app\common\model\server\Setting;
use app\common\model\server\Chats;
use app\common\model\server\Robot;
use app\common\model\server\Sentence;
use app\common\model\server\Banword;
use app\common\model\server\Comment;
use app\common\model\system\SysUser;
use \GatewayWorker\Lib\Gateway;
use app\api\controller\Event;
use app\admin\logic\server\ConsumerLogic;
use app\admin\logic\server\RobotLogic;
use app\admin\logic\server\SettingLogic;
use app\admin\logic\server\SentenceLogic;
use app\admin\logic\server\ChatsLogic;
use app\admin\logic\server\BanwordLogic;
use app\admin\logic\system\SysUserLogic;
/**
 * 跨域公用控制器.
 */
class Server
{
    public function test(){
        $server=SysUserLogic::getAll(['state'=>1]);
        $keys = array_column($server->toArray(), 'id'); // 假设'key_field'是你要搜索的键的名称
        var_dump($keys);
        $num=array_search(6, $keys);
        var_dump($num);die;
    }
//     public function createUid($client_id){
//         $data['type']='home';
//         $data['uid']=bin2hex(pack('N', time())).strtolower(rands(8));
//         Event::sendClient($client_id,$data);
//     }
//     //进行评论
//     public function comment($data){
// 		Comment::create($data);
//     }
//     //修改客服或用户离线状态
//     public function editOnline($client_id){
//         //var_dump(session($client_id));die;
//         $data=!empty(session[$client_id]) ? session[$client_id] : '';
//         if(!$data) return false;
//         if($data['type']==1){
//             $visiter=ConsumerLogic::detail(['uid'=>$data['uid']]);
//             ConsumerLogic::editConsumer(['id'=>$visiter['id'],'is_online'=>0]);//修改访客状态
//             if($visiter['kefu_id']) $this->sendOnlineList($visiter['kefu_id']);//通知客服
//         }
//         if($data['type']==2){
//             SysUserLogic::editUser(['id'=>$data['id'],'state'=>2]);//修改客服状态
//             Event::offGroup($client_id,'serverlist');
//         }
//     }
//     public function home($client_id,$data,$ip){
//         $user=ConsumerLogic::detail(['uid'=>$data['uid']]);
//         //绑定UID
//         Event::addUid($client_id,$data['uid']);
//          //黑名单
//         if(!empty($user) && $user['state']==3){
//             return Event::sendUid($data['uid'],['type'=>'no_server','msg'=>'您已被加入黑名单，禁止访问']);
//         }
//         //修改访客状态
//         if(!empty($user) && $user['is_online']==0){
//             ConsumerLogic::editConsumer(['id'=>$user['id'],'is_online'=>1,'login_num'=>$user['login_num']+1]);//修改访客状态
//             if($user['kefu_id']) $this->sendOnlineList($user['kefu_id']);
//         } 
//         //用户不存在写入数据库
//         if(!$user){
//             $data['ip']=$ip;
//             $user=ConsumerLogic::editConsumer($data);
//         }
//         $user->type=1;
//         //$_SESSION[$client_id]=$user->toArray();
//         session($client_id,$user->toArray());
//         //问答列表
//         $robot=RobotLogic::getreply(['type'=>1]);
//         Event::sendUid($data['uid'],['type'=>'robotlist','msg'=>'问答列表','data'=>$robot]);
//         //查询客服设置
//         $setting=$this->setting($data['uid']);
//         if($setting)
//           $this->serverList($user,$setting,$client_id);
//     }
//     public function setting($uid){
//         $setting=SettingLogic::detail(['id'=>1]);
//         if($setting['is_off']==2) 
//             return Event::sendUid($uid,['type'=>'no_server','msg'=>'没有客服在线，请留言1']);
//         $time=explode('-',str_replace(':','',$setting['worktime']));
//         if($time[0]<$time[1] && (date('His')<$time[0] || date('His')>$time[1]) || ($time[0]>$time[1] && date('His')<$time[0] && date('His')>$time[1])){
//             return Event::sendUid($uid,['type'=>'no_server','msg'=>'不在工作时间，请留言1']);
//         }
//         return $setting;
//     }
//     public function serverList($user,$setting,$client_id){
//         $server=SysUserLogic::getAll(['state'=>1]);
//         if(count($server)==0) 
//             return Event::sendUid($user['uid'],['type'=>'no_server','msg'=>'没有客服在线，请留言2']);
//         $kefu=[];
//         foreach($server as $k=>$v){
//             if($user['kefu_id']==$v['id']){
//                 $kefu=$v;
//                 break;
//             }
//         }
//         if($kefu) return $this->getSentence($kefu['id'],$user['uid'],$kefu['is_reply']);
//         if($setting['is_auto']==1){ //智能分配
//             $rand=rand(0,count($server)-1);
//             $this->bindServer($user,$server[$rand]['id']);
//             $this->getSentence($server[$rand]['id'],$user['uid'],$server[$rand]['is_reply']);
//             return $this->sendOnlineList($server[$rand]['id']);
//         }
//         if($setting['is_auto']==2){ //进入客服排队列表
//             $pdlist=ConsumerLogic::getAll(['kefu_id'=>NULL]);
//             Event::sendGroup('serverlist',['type'=>'pdlist','msg'=>'排队列表','pdlist'=>$pdlist]);
//             $num=0;
//             foreach($pdlist as $k=>$v){
//                 $num=$num+1;
//                 if($v['uid']==$user['uid']){
//                     break;
//                 }
//             }
//             Event::addGroup($client_id,'pdlist');
//             $msglist=$this->getMsgList($user['uid']);
//             return Event::sendUid($user['uid'],['type'=>'pdmsg','msg'=>'正在排队','data'=>$num,'msglist'=>$msglist]);
//         }
//         return Event::sendUid($user['uid'],['type'=>'serverlist','msg'=>'客服列表','data'=>$server]);
//     }
    
//     //绑定客服
//     public function bindServer($user,$kefu_id){
//         return ConsumerLogic::editConsumer(['id'=>$user['id'],'kefu_id'=>$kefu_id]);
//     }
//     //发送欢迎语
//     public function getSentence($kefu_id,$uid,$is_reply=1){
//         if($is_reply==3) return false;
//         $sentence=SentenceLogic::getAll(['kefu_id'=>$kefu_id]);
//         if(count($sentence)==0) return false;
//         $rand= count($sentence)>0 && $is_reply==2 ? rand(0,count($sentence)-1) : 0;
//         //查询历史记录
//         $msglist=$this->getMsgList($uid);
//         $server=SysUserLogic::details(['id'=>$kefu_id]);
//         return Event::sendUid($uid,['type'=>'welcome','msg'=>'欢迎语','data'=>$sentence[$rand]['content'],'time'=>time(),'kefu_id'=>$kefu_id,'msglist'=>$msglist,'server'=>$server]);
//     }
//     //聊天历史记录
//     public function getMsgList($uid){
//         return ChatsLogic::getAll(['uid'=>$uid]);
//     }
//     //用户前端点击绑定客服
//     public function selectServer($data){
//         Consumer::where('uid',$data['uid'])->update(['kefu_id'=>$data['kefu_id']]);
//         $this->getSentence($data['kefu_id'],$data['uid']); 
//     }
//     //给客服提前发送消息
//     public function sendValue($data){
//         Event::sendUid($data['kefu_id'], ['type'=>'sendvalue','msg'=>'成功','data'=>$data]);
//     }
//     //撤销消息
//     public function cancel($data){
//         Chats::where('unstr', $data['unstr'])->delete();
//         Event::sendUid($data['uid'], ['type'=>'cancel','msg'=>'成功','data'=>$data['unstr']]);
//     }
//     //发送消息
//     public function sendmsg($data){
// 		$data['content']=BanwordLogic::replaceMsg(trim($data['content']));
// 		//入库数据
// 		unset($data['type']);
// 		ChatsLogic::edit($data);
//         $uid=$data['direction']==1 ? $data['kefu_id'] : $data['uid'];
//         if($uid){
//             $this->addServerList($data);
//             Event::sendUid($uid, ['type'=>'msg','msg'=>'成功','data'=>$data]);
//         }
//         if($data['direction']==3){ //转接客服
//             $server=SysUser::field('id,nickname,avatar')->where('id',$data['switch_id'])->find();
//             Event::sendUid($data['uid'], ['type'=>'serverdetail','msg'=>'成功','data'=>$server]);
//             $this->sendOnlineList($data['switch_id']);
//         }
//         //加入排队列表
//         if(!$data['kefu_id']){
//             $this->addPdList($data);
//         }
//     }
//     //在线离线列表
//     public function sendOnlineList($kefu_id){
//         $onlinelist=ConsumerLogic::getAll(['kefu_id'=>$kefu_id,'is_online'=>1],'istop desc');
//         $offlinelist=ConsumerLogic::getAll(['kefu_id'=>$kefu_id,'is_online'=>0],'istop desc');
//         Event::sendUid($kefu_id,['type'=>'onlinelist','msg'=>'在线离线列表','onlinelist'=>$onlinelist,'offlinelist'=>$offlinelist]); 
//     }
//     //排队列表
//     public function addPdList($data){
//         $queueList=ConsumerLogic::getAll(['kefu_id'=>NULL],'id asc');
//         $uidList=array_column($queueList->toArray(),'uid');
//         Event::sendGroup('pdlist',['type'=>'pdlist','msg'=>'成功','pdlist'=>$uidList]);
//         Event::sendGroup('serverlist',['type'=>'serverlist','msg'=>'成功','pdlist'=>$queueList]);
//     }
//     //客服登录
//     public function serverLogin($client_id,$data){
//         $service=SysUserLogic::details(['id'=>$data['kefu_id']]);
//         if($service['state']==2) SysUserLogic::editUser(['id'=>$data['kefu_id'],'state'=>1]); //修改客服状态
//         Event::addUid($client_id,$data['kefu_id']); //绑定ID
//         $service->type=2;
//         session[$client_id]=$service->toArray();
//         Event::addGroup($client_id,'serverlist'); //加入群组
//     }
//     //加入移出黑名单
//     public function blackList($data){
//         $msg= $data['type']==3 ? '您已被加入黑名单' : '您已被移出黑名单';
//         Consumer::where('uid',$data['uid'])->update(['state'=>$data['type']]);
//         Event::sendUid($data['uid'], ['type'=>'black','msg'=>$msg]);
//     }
//     //加入在线列表
//     public function addServerList($data){
//         $onlinelist=ConsumerLogic::getAll(['kefu_id'=>$data['kefu_id'],'is_online'=>1],'istop desc');
//         return Event::sendUid($data['kefu_id'], ['type'=>'onlinelist','msg'=>'在线列表','onlinelist'=>$onlinelist]);
//     }
//     //删除在线列表
//     public function delServerList($data){
//         $this->addServerList($data);
//     }
}