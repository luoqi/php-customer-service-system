<?php

declare(strict_types=1);

namespace app\api\controller;
use app\common\controller\Base;
use app\admin\logic\UploadLogic;
/**
 * 文件上传
 * Class Upload
 * @package app\admin\controller\system
 */
class Upload extends Base
{

    /**
     * 图片文件上传（单文件）
     */
    public function uploadImage()
    {
       
        $this->validate($this->request->file(), [
            'file' => 'file|fileExt:jpg,png,jpeg,mp4'
        ], [
            'file.file' => '请选上传文件', 'file.fileExt' => '系统仅支持 jpg,png,jpeg 图片格式,mp4视频'
        ]);
        return $this->success(UploadLogic::uploadImage());
    }
    /**
     * 附件文件上传（单文件）
     */
    public function uploadFile()
    {
        $this->validate($this->request->file(), [
            'file' => 'file|fileExt:xls,xlsx,doc'
        ], [
            'file.file' => '请选上传文件', 'file.fileExt' => '系统仅支持 xls,xlsx,doc格式文件'
        ]);
        $path = $this->request->param('type') ?? 'files';
        $file = $this->request->file('file');
        return $this->success(UploadLogic::uploadFile());
    }
}
