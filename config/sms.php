<?php
// +----------------------------------------------------------------------
// | 胜家云 [ SingKa Cloud ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.singka.net All rights reserved.
// +----------------------------------------------------------------------
// | 宁波晟嘉网络科技有限公司
// +----------------------------------------------------------------------
// | Author: ShyComet <shycomet@qq.com>
// +----------------------------------------------------------------------
return [
    'aliyun'       => [
        'version'       => '2017-05-25',
        'host'          => 'dysmsapi.aliyuncs.com',
        'scheme'        => 'http',
        'region_id'     => 'cn-hangzhou',
        'access_key'    => '',
        'access_secret' => '',
        'sign_name'     => '',
        'actions'       => [
            'register'        => [
                'actions_name'      => '注册验证',
                'template_name'=>'您的注册验证码是{code}',
                'template_id'  => '',
            ],
            'login'           => [
                'actions_name'      => '登录验证',
                'template_name'=>'您的登录验证码是{code}',
                'template_id'  => '',
            ]
        ],
    ],
    'smsbao'       => [
        'title'=>'',
        'content'   =>  '您的验证码是{code}',
        'account'  =>  '',
        'password'   =>  '',
        'base_url'     =>  'http://api.smsbao.com/',
        'actions'       => [
            'register'        => [
                'actions_name'      => '注册验证',
                'template_name'=>'',
            ],
            'login'           => [
                'actions_name'      => '登录验证',
                'template_name'=>'',
            ]
        ],
    ],
    'qcloud'       => [
        'appid'   =>  '',
        'appkey'  =>  '',
        'sign_name'       => '',
        'actions'       => [
            'register'        => [
                'actions_name'      => '注册验证',
                'template_name'=>'您的注册验证码是{code}',
                'template_id'  => '',
            ],
            'login'           => [
                'actions_name'      => '登录验证',
                'template_name'=>'您的登录验证码是{code}',
                'template_id'  => '',
            ]
        ],
    ],
    'qiniu'       => [
        'AccessKey'   =>  '',
        'SecretKey'  =>  '',
        'actions'       => [
            'register'        => [
                'actions_name'      => '注册验证',
                'template_name'=>'您的注册验证码是{code}',
                'template_id'  => '',
            ],
            'login'           => [
                'actions_name'      => '登录验证',
                'template_name'=>'您的登录验证码是{code}',
                'template_id'  => '',
            ]
        ],
    ],
    'huawei'       => [
        'url'  =>  '',
        'appKey'   =>  '',
        'appSecret'  =>  '',
        'sender'  =>  '',
        'signature'  =>  '',
        'statusCallback'  =>  '',
        'actions'       => [
            'register'        => [
                'actions_name'      => '注册验证',
                'template_name'=>'',
                'template_id'  => '',
            ],
            'login'           => [
                'actions_name'      => '登录验证',
                'template_name'=>'',
                'template_id'  => '',
            ]
        ],
    ]
];