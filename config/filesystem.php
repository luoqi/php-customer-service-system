<?php

return [
    // 默认磁盘
    'default' => 'local',
    // 磁盘列表
    'disks'   => [
        'local'  => [
            'driver' => \app\common\service\storage\Local::class,
            'type' => 'local',
            'root' => app()->getRootPath() . 'public/storage',
            'url' => '/storage',//磁盘路径对应的外部URL路径
            'visibility' => 'public', //可见性
            'domain' => '',
        ],
        'public' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/storage',
            // 磁盘路径对应的外部URL路径
            'url'        => '/storage',
            // 可见性
            'visibility' => 'public',
        ],
        'aliyun' => [
            'driver' => \app\common\service\storage\Aliyun::class,
        ],
        'qiniu' => [
            'driver' =>  \app\common\service\storage\Qiniu::class,
        ],
        // 更多的磁盘配置信息
    ],
    'file' => [
        'suffix' => '.jpg,.png,.bmp,.jpeg,.gif,.zip,.rar,.xls,.xlsx,.mp3,.mp4,.doc',
        'maxSize' => '20', //最大上传20M
    ]
];
